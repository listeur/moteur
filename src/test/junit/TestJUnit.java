package test.junit;

import static org.junit.Assert.*;

import java.io.File;

import moteur.DPath;
import moteur.outils.O;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestJUnit {
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		O.sop("début des tests dans Moteur");
	}
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		O.sop("fin des tests dans Moteur");
	}

	@Before
	public void setUp() throws Exception {
	}
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		//fail("Pas encore implémenté");
		assertNotNull("Pas encore implémenté");
	}

	@Test
	public void testPath() {
		File f=DPath.dPath(".");
		assertTrue(f.getAbsolutePath(), f.getAbsolutePath().contains("Moteur"));
	}
	@Test
	public void testDPathEnglais() {
		File f=DPath.dPath("/local/en.xml");
		assertTrue(f.getAbsolutePath(), f.getAbsolutePath().contains("Moteur"));
	}
	@Test
	public void testDPathFrancais() {
		File f=DPath.dPath("/local/fr.xml");
		assertTrue(f.getAbsolutePath(), f.getAbsolutePath().contains("Moteur"));
	}
}
