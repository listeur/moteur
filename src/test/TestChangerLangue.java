package test;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * TODO : ajouter un test pour les tooltypes
 * 
 */
public class TestChangerLangue {
	Map<String, List<JComponent>> componentsmap;
	String bundlename;
	Locale locale;
	ResourceBundle bundle;
	Locale[] langlist={Locale.ENGLISH, Locale.FRENCH};
	int langid=0;

	public static void main(String[] args) {
		final TestChangerLangue lc=new TestChangerLangue("test.ressources.LangChanger");
		JFrame jf=new JFrame("Testing Frame");
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setPreferredSize(new Dimension(300, 100));
		JLabel label=new JLabel("this_is_a_label");
		JButton jb=new JButton("Change_language");
		jb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				lc.nextLang();
			}
		});
		jf.getContentPane().setLayout(new FlowLayout());
		jf.getContentPane().add(label);
		jf.getContentPane().add(jb);
		lc.build(jf);
		lc.nextLang();
		jf.setVisible(true);
		jf.pack();
	}

	public TestChangerLangue(String bundlename) {
		this.bundlename=bundlename;
		locale=Locale.getDefault();
		ResourceBundle.clearCache();
		bundle=ResourceBundle.getBundle(bundlename, locale);
		componentsmap=new HashMap<String, List<JComponent>>();
	}

	void build(Component jc) {
		if(jc instanceof JFrame) {
			Container cont=((JFrame) jc).getContentPane();
			int count=cont.getComponentCount();
			for(int i=0; i < count; i++) {
				build(cont.getComponent(i));
			}
		}

		// other work before in case of JFrame
		if(jc instanceof JPanel || jc instanceof JFrame) {
			int count=((Container) jc).getComponentCount();
			for(int i=0; i < count; i++) {
				build(((Container) jc).getComponent(i));
			}
			return;
		}

		if(jc instanceof JComponent) {
			String text=null;
			if(jc instanceof JLabel) {
				text=((JLabel) jc).getText();
				System.out.println("add a JLabel with text = " + text);
			}
			if(jc instanceof JButton) {
				text=((JButton) jc).getText();
				System.out.println("add a JButton with text = " + text);
			}
			if(text == null) {
				return;
			}
			if(!componentsmap.containsKey(text)) {
				componentsmap.put(text, new ArrayList<JComponent>());
			}
			componentsmap.get(text).add((JComponent) jc);
		}
	}

	void changLang(int id) {
		locale=langlist[id];
		//ResourceBundle.clearCache();
		bundle=ResourceBundle.getBundle(bundlename, locale);
		reload();
	}

	void reload() {
		//System.out.println("Loading another language to : "  + locale);
		Iterator<String> key_it=componentsmap.keySet().iterator();
		while(key_it.hasNext()) {
			String key=key_it.next();
			Iterator<JComponent> it=componentsmap.get(key).iterator();
			String text=bundle.getString(key);
			while(it.hasNext()) {
				JComponent comp=it.next();
				if(comp instanceof JLabel) {
					//System.out.println("JL to " + text);
					((JLabel) comp).setText(text);
				}
				if(comp instanceof JButton) {
					//System.out.println("JB to " + text);
					((JButton) comp).setText(text);
				}

			}
		}
	}

	void nextLang() {
		langid++;
		langid%=langlist.length;
		changLang(langid);
	}
}
