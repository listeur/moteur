package test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import moteur.outils.O;

public class TestRegex {
	public static void main(String[] args) {
		toTest();
		toTest2();
	}
	
	public static void toTest() {
		String s="L1MEM2";

		Pattern pattern=Pattern.compile("([-a-zA-Z]+)([\\d]+)([-a-zA-Z]+)([\\d]+)");
		Matcher matcher=pattern.matcher(s);
		if(matcher.find()) {
			for(int i=0; i<=matcher.groupCount(); i++) {
				O.sop(matcher.group(i));
			}
		} else {
			O.sop("RAS");
		}
	}
	
	public static void toTest2() {
		String s="L1MEM2";

		Pattern pattern=Pattern.compile("([-a-z]+)([\\d]+)([-a-zA-Z]+)([\\d]+)", Pattern.CASE_INSENSITIVE);
		Matcher matcher=pattern.matcher(s);
		if(matcher.find()) {
			for(int i=0; i<=matcher.groupCount(); i++) {
				O.sop(matcher.group(i));
			}
		} else {
			O.sop("RAS");
		}
	}
}
