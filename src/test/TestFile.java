package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

import moteur.outils.O;

public class TestFile {
	public static void main(String[] args) throws Exception {

		File ftest=new File(".");
		ftest=ftest.getAbsoluteFile();
		O.sop(ftest.getPath());
		O.sop(O.voirArray(ftest.list()));

		File sortie=new File("xmlTest.xml");
		O.sop(sortie);

		FileOutputStream fos=new FileOutputStream(sortie);

		Properties prop=new Properties();
		prop.put("test", "valeur");
		prop.put("Fichier", "Fichier");
		prop.put("Compliqué", "1=2");
		prop.storeToXML(fos, null);
		prop.store(new FileOutputStream("configTest.ini"), null);
		O.sop(prop);

		FileInputStream fis=new FileInputStream(sortie);

		Properties prop2=new Properties();
		prop2.loadFromXML(fis);
		O.sop(prop2);
		
		sortie.delete();
		new File("configTest.ini").delete();
	}
}
