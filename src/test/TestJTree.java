package test;

import java.awt.Color;
import java.awt.Font;
import java.io.File;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

public class TestJTree {
	public static void main(String[] args) {
		exempleTree3();
	}

	static DefaultMutableTreeNode racine=new DefaultMutableTreeNode("");
	static DefaultTreeModel tm=new DefaultTreeModel(racine);
	final static JTree monArbreF=new JTree(tm);

	public static void exempleTree3() {
		final JFrame jf=new JFrame();
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// construction de la liste des lecteurs
		File[] lecteurs=File.listRoots();
		for(int i=0; i<lecteurs.length; ++i) {
			DefaultMutableTreeNode nouveau=new DefaultMutableTreeNode(lecteurs[i].toString());
			racine.add(nouveau);
		}

		// modification du rendu de l’arbre
		DefaultTreeCellRenderer renderer=new DefaultTreeCellRenderer();
		renderer.setLeafIcon(null);
		renderer.setBorderSelectionColor(Color.RED);
		renderer.setBackgroundSelectionColor(Color.WHITE);
		renderer.setTextSelectionColor(Color.RED);
		renderer.setFont(new Font("Comic sans ms", Font.BOLD, 14));
		renderer.setTextNonSelectionColor(Color.BLUE);
		monArbreF.setCellRenderer(renderer);
		// déploiement du sous arbre d’un nœud sélectionné
		monArbreF.addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
				DefaultMutableTreeNode lenoeud=(DefaultMutableTreeNode) monArbreF.getLastSelectedPathComponent();
				if(lenoeud==null)
					return;
				developper(lenoeud);
				jf.pack();
			}
		});

		jf.setContentPane(new JScrollPane(monArbreF));
		jf.pack();
		jf.setVisible(true);
	}

	static void developper(DefaultMutableTreeNode n) {
		// construction du chemin depuis la racine
		TreeNode[] paths=n.getPath();
		String path="";
		for(int i=0; i<paths.length; ++i) {
			if(paths[i].toString()!=null)
				path+="\\"+paths[i];
			File f=new File(path.substring(1));
			// si le nœud  est un repertoire
			// contenant au moins un fichier
			// créer la liste des fichiers du répertoire
			if(f.isDirectory()&&n.getChildCount()==0) {
				File tf[]=f.listFiles();
				for(int i1=0; i1<tf.length; ++i1) {
					tm.insertNodeInto(new DefaultMutableTreeNode(tf[i1].getName()), n, i1);
					monArbreF.expandPath(new TreePath(paths));
				}
			}
		}
	}
}
