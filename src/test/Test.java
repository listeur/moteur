package test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import moteur.outils.Bench;

public class Test {
	public static void main(String[] args) throws Exception {
		Bench b;
		int i=1000;
		/*
		b=new Bench();
		b.boucle(i, new Test(), "toTest41");
		b.kill();

		b=new Bench();
		b.boucle(i, new Test(), "toTest41");
		b.kill();

		b=new Bench();
		b.boucle(i, new Test(), "toTest42");
		b.kill();

		b=new Bench();
		b.boucle(i, new Test(), "toTest42");
		b.kill();

		b=new Bench();
		b.boucle(i, new Test(), "toTest43");
		b.kill();

		b=new Bench();
		b.boucle(i, new Test(), "toTest43");
		b.kill();

		b=new Bench();
		b.boucle(i, new Test(), "toTest44");
		b.kill();

		b=new Bench();
		b.boucle(i, new Test(), "toTest44");
		b.kill();
		//*/

		b=new Bench();
		b.boucle(i, new Test(), "toTest51");
		//b.kill();
		
		b=new Bench();
		b.boucle(i, new Test(), "toTest52");
		//b.kill();
		
		b=new Bench();
		b.boucle(i, new Test(), "toTest51");
		//b.kill();

		b=new Bench();
		b.boucle(i, new Test(), "toTest52");
		//b.kill();
		//*/
	}

	public static void toTest() {
		String s="L1MEM2";

		Pattern pattern=Pattern.compile("([-a-zA-Z]+)([\\d]+)([-a-zA-Z]+)([\\d]+)");
		Matcher matcher=pattern.matcher(s);
		if(matcher.find()) {
			for(int i=0; i <= matcher.groupCount(); i++) {
				//O.sop(matcher.group(i));
			}
		} else {
			//O.sop("RAS");
		}
	}

	public static void toTest2() {
		String s="L1MEM2";

		Pattern pattern=Pattern.compile("([-a-z]+)([\\d]+)([-a-zA-Z]+)([\\d]+)", Pattern.CASE_INSENSITIVE);
		Matcher matcher=pattern.matcher(s);
		if(matcher.find()) {
			for(int i=0; i <= matcher.groupCount(); i++) {
				//O.sop(matcher.group(i));
			}
		} else {
			//O.sop("RAS");
		}
	}

	public static void toTest3() {
		String s="L1MEM2";

		Pattern pattern=Pattern.compile("([-A-Z]+)([\\d]+)([-a-zA-Z]+)([\\d]+)", Pattern.CASE_INSENSITIVE);
		Matcher matcher=pattern.matcher(s);
		if(matcher.find()) {
			for(int i=0; i <= matcher.groupCount(); i++) {
				//O.sop(matcher.group(i));
			}
		} else {
			//O.sop("RAS");
		}
	}


	public static void toTest41() {
		String s="DICT¤ana4_min";

		Pattern pattern=Pattern.compile("([-a-zA-Z¤]+)([\\d]+)([-a-zA-Z_]+)");
		Matcher matcher=pattern.matcher(s);
		if(matcher.find()) {
			for(int i=0; i <= matcher.groupCount(); i++) {
				//O.sop(matcher.group(i));
			}
		} else {
			//O.sop("RAS");
		}
	}
	public static void toTest42() {
		String s="DICT¤ana4_min";

		Pattern pattern=Pattern.compile("([-a-zA-Z]+)([\\d]+)([-a-zA-Z_]+)");
		Matcher matcher=pattern.matcher(s);
		if(matcher.find()) {
			for(int i=0; i <= matcher.groupCount(); i++) {
				//O.sop(matcher.group(i));
			}
		} else {
			//O.sop("RAS");
		}
	}
	public static void toTest43() {
		String s="DICT¤ana4_min";

		Pattern pattern=Pattern.compile("([-a-zA-Z¤]+)([\\d]+)([-a-zA-Z]+)");
		Matcher matcher=pattern.matcher(s);
		if(matcher.find()) {
			for(int i=0; i <= matcher.groupCount(); i++) {
				//O.sop(matcher.group(i));
			}
		} else {
			//O.sop("RAS");
		}
	}
	public static void toTest44() {
		String s="DICT¤ana4_min";

		Pattern pattern=Pattern.compile("([-a-zA-Z]+)([\\d]+)([-a-zA-Z]+)");
		Matcher matcher=pattern.matcher(s);
		if(matcher.find()) {
			for(int i=0; i <= matcher.groupCount(); i++) {
				//O.sop(matcher.group(i));
			}
		} else {
			//O.sop("RAS");
		}
	}
	
	public static void toTest51() {
		String s="EXCEPTION :\nclass java.io.FileNotFoundException : a pas fichier\ntest.Exec$1.run(Exec.java:122)\njava.awt.event.InvocationEvent.dispatch(Unknown Source)\njava.awt.EventQueue.dispatchEventImpl(Unknown Source)\njava.awt.EventQueue.access$000(Unknown Source)\njava.awt.EventQueue$3.run(Unknown Source)\njava.awt.EventQueue$3.run(Unknown Source)\njava.security.AccessController.doPrivileged(Native Method)\njava.security.ProtectionDomain$1.doIntersectionPrivilege(Unknown Source)\njava.awt.EventQueue.dispatchEvent(Unknown Source)\njava.awt.EventDispatchThread.pumpOneEventForFilters(Unknown Source)\njava.awt.EventDispatchThread.pumpEventsForFilter(Unknown Source)\njava.awt.EventDispatchThread.pumpEventsForHierarchy(Unknown Source)\njava.awt.EventDispatchThread.pumpEvents(Unknown Source)\njava.awt.EventDispatchThread.pumpEvents(Unknown Source)\njava.awt.EventDispatchThread.run(Unknown Source)";
		s=s.replace("\n", "\r\n");
	}
	public static void toTest52() {
		String s="EXCEPTION :\nclass java.io.FileNotFoundException : a pas fichier\ntest.Exec$1.run(Exec.java:122)\njava.awt.event.InvocationEvent.dispatch(Unknown Source)\njava.awt.EventQueue.dispatchEventImpl(Unknown Source)\njava.awt.EventQueue.access$000(Unknown Source)\njava.awt.EventQueue$3.run(Unknown Source)\njava.awt.EventQueue$3.run(Unknown Source)\njava.security.AccessController.doPrivileged(Native Method)\njava.security.ProtectionDomain$1.doIntersectionPrivilege(Unknown Source)\njava.awt.EventQueue.dispatchEvent(Unknown Source)\njava.awt.EventDispatchThread.pumpOneEventForFilters(Unknown Source)\njava.awt.EventDispatchThread.pumpEventsForFilter(Unknown Source)\njava.awt.EventDispatchThread.pumpEventsForHierarchy(Unknown Source)\njava.awt.EventDispatchThread.pumpEvents(Unknown Source)\njava.awt.EventDispatchThread.pumpEvents(Unknown Source)\njava.awt.EventDispatchThread.run(Unknown Source)";
		s=s.replaceAll("\n", "\r\n");
	}
}
