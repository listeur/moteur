package test;

import java.util.Locale;

import moteur.outils.O;

public class TestLocale {
	public static void main(String[] args) {
		Locale loc=new Locale("fr", "FR", "Windows");
		//loc=Locale.FRANCE;
		O.sop(loc.getCountry());				// FR
		O.sop(loc.getDisplayCountry());			// France
		O.sop(loc.getDisplayLanguage());		// français
		O.sop(loc.getDisplayName());			// français (France,Windows)
		O.sop(loc.getDisplayScript());			// 
		O.sop(loc.getDisplayVariant());			// Windows
		O.sop(loc.getISO3Country());			// FRA
		O.sop(loc.getISO3Language());			// fra
		O.sop(loc.getLanguage());				// fr
		O.sop(loc.toLanguageTag());				// fr-FR-Windows
		O.sop(loc.toString());					// fr_FR_Windows
	}
}
