package test;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import moteur.vue.JF;
import moteur.vue.JP;
import moteur.vue.JSP;
import moteur.vue.JTA;
import moteur.vue.JTF;

public class TestDragDrop extends JF {
	private static final long serialVersionUID=1L;

	public TestDragDrop() {
		super("Test de Drag'n Drop");
		setSize(300, 210);

		JP pan=new JP();
		pan.setBackground(Color.white);
		pan.setLayout(new BorderLayout());

		//Notre textearea avec son contenu déplaçable
		JTA label=new JTA("Texte déplaçable !");
		label.setPreferredSize(new Dimension(300, 130));
		//--------------------------------------------------
		//C'est cette instruction qui permet le déplacement 
		//de son contenu
		label.setDragEnabled(true);
		//--------------------------------------------------

		pan.add(new JSP(label), BorderLayout.NORTH);

		JP pan2=new JP();
		pan2.setBackground(Color.white);
		pan2.setLayout(new BorderLayout());

		//On crée le premier textfield avec contenu déplaçable
		JTF text=new JTF();
		//--------------------------------------------------
		text.setDragEnabled(true);
		//--------------------------------------------------
		//Et le second, sans.
		JTF text2=new JTF();

		pan2.add(text2, BorderLayout.SOUTH);
		pan2.add(text, BorderLayout.NORTH);

		pan.add(pan2, BorderLayout.SOUTH);
		add(pan, BorderLayout.CENTER);

		setVisible(true);
	}
	
	public static void main(String[] args) {
		new TestDragDrop();
	}
}
