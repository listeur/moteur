package moteur.outils;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import moteur.Constantes;

@SuppressWarnings("unchecked")
public final class O implements Constantes {
	private static final String STATIC_LOG=LOG_FOLDER + S + "out" + new Date().getTime() + ".log";
	private static final Path STATIC_LOG_PATH=Paths.get(STATIC_LOG);

	// Méthodes
	public static final void sop(Object o) {
		System.out.println(o);
	}
	public static final void sopp(Object o) {
		System.out.print(o);
	}

	public static final void fop(Object o) {
		fop(o, STATIC_LOG_PATH);
	}
	public static final void fop(Object o, String path) {
		fop(o, Paths.get(path));
	}
	public static final void fop(Object o, Path path) {
		try (PrintWriter pw=new PrintWriter(Files.newBufferedWriter(path, StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND))) {
			pw.println(o);
		} catch(IOException e) {
			O.sop("Impossible d'écrire le fileOutPutPrintLn");
		}
	}
	public static final void fopp(Object o) {
		fopp(o, STATIC_LOG_PATH);
	}
	public static final void fopp(Object o, String path) {
		fopp(o, Paths.get(path));
	}
	public static final void fopp(Object o, Path path) {
		try (PrintWriter pw=new PrintWriter(Files.newBufferedWriter(path, StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND))) {
			pw.print(o);
		} catch(IOException e) {
			O.sop("Impossible d'écrire le fileOutPutPrint");
		}
	}

	public static final <T> String voirArray(T[] tab) {
		return voirArray(tab, false);
		//return Arrays.toString(tab);
	}
	public static final <T> String voirArray(T[] tab, boolean echappeVir) {
		StringBuilder s=new StringBuilder();
		boolean vir=false;
		String nom="Inconnu";
		try {
			T tt=tab[0];
			nom=tt.getClass().getCanonicalName();
		} catch(ArrayIndexOutOfBoundsException e) {
		}
		s.append("Array (" + nom + ")\n");
		int i=0;
		for(T t : tab) {
			s.append(i + " ");
			s.append("[");
			if(t.getClass().isArray()) {
				s.append(voirSousArray((T[]) t));
			} else {
				if(echappeVir) {
					vir=t.toString().contains(",");
					s.append((vir?"\"":""));
				}
				s.append(t);
				s.append((vir && echappeVir?"\"":""));
			}
			s.append("]\n");
			i++;
		}
		return s.toString();
	}
	private static final <T> StringBuilder voirSousArray(T[] tab) {
		StringBuilder s=new StringBuilder("[");
		for(T t : tab) {
			//s.append((s.length() != 1?",":""));
			if(t.getClass().isArray()) {
				s.append(voirSousArray((T[]) t));
			} else {
				s.append(t);
				s.append(",");
			}
		}
		s.deleteCharAt(s.lastIndexOf(","));
		return s.append("]");
	}

	public static final <T> String voirArray(Collection<T> tab) {
		StringBuilder s=new StringBuilder();
		s.append("Array Collection (" + tab.getClass().getCanonicalName() + ")\n");
		/*s+="Array Collection";
		if(!tab.isEmpty()) {
			s+=" ("+tab.toArray()[0].getClass().getCanonicalName()+")";
		}
		s+="\n";*/
		int i=0;
		for(T t : tab) {
//			boolean vir=t.toString().contains(",");
			s.append(i + " ");
			s.append("[");
//			s+="["+(vir?"\"":"");
			s.append(t);
			s.append("]\n");
//			s+=(vir?"\"":"")+"]\n";
			i++;
		}
		return s.toString();
	}

	public static final <K, V> String voirArray(Map<K, V> tab) {
		StringBuilder s=new StringBuilder();
		s.append("Array Map (" + tab.getClass().getCanonicalName() + ")\n");
		Set<K> set=tab.keySet();
		/*s+="Array Map";
		Set<K> set=tab.keySet();
		if(!tab.isEmpty()) {
			K k=set.iterator().next();
			s+=" ("+k.getClass().getCanonicalName()+","+tab.get(k).getClass().getCanonicalName()+")";
		}
		s+="\n";*/
		for(K fs : set) {
//			boolean vir=tab.get(fs).toString().contains(",");
			s.append(fs + " ");
			s.append("[");
//			s+="["+(vir?"\"":"");
			s.append(tab.get(fs));
			s.append("]\n");
//			s+=(vir?"\"":"")+"]\n";
		}
		return s.toString();
	}


	/**
	 * Change un tableau en ArrayList.
	 * 
	 * @param <T> type des objects contenu dans le tableau
	 * @param tab tableau à changer
	 * @return un ArrayList<T> correspondant au tableau
	 */
	public static final <T> ArrayList<T> tabToArrayList(T[] tab) {
		/*ArrayList<T> ar=new ArrayList<>();
		for(T t : tab) {
			ar.add(t);
		}
		return ar;*/
		return new ArrayList<T>(Arrays.asList(tab));
	}

	/**
	 * Change un tableau en 2 dimension en ArrayList d'ArrayList.
	 * 
	 * @param <T> type des objects contenu dans le tableau
	 * @param tab tableau à changer
	 * @return un ArrayList<ArrayList<T>> correspondant au tableau
	 */
	public static final <T> ArrayList<ArrayList<T>> tabToArrayList(T[][] tab) {
		ArrayList<ArrayList<T>> ar=new ArrayList<>();
		for(T[] t : tab) {
			/*ArrayList<T> ar2=new ArrayList<>();
			for(T t2 : t) {
				ar2.add(t2);
			}
			ar.add(ar2);*/
			ar.add(tabToArrayList(t));
		}
		return ar;
	}

	/**
	 * Change un ArrayList d'ArrayList en ArrayList de tableau.
	 * 
	 * @param <T> type des objects contenu dans l'ArrayList<ArrayList<T>>
	 * @param tab ArrayList à tranformer
	 * @return un ArrayList<T[]> correspondant à l'ArrayList<ArrayList<T>>
	 */
	public static final <T> ArrayList<T[]> arrayArrayListToArrayListTab(ArrayList<ArrayList<T>> tab) {
		ArrayList<T[]> ar=new ArrayList<>();
		for(ArrayList<T> t : tab) {
			T[] t2=(T[]) t.toArray();
			ar.add(t2);
		}
		return ar;
	}
	/**
	 * Change un ArrayList d'ArrayList en tableau.
	 * Attention : les sous ArrayKist doivent tous avoir la même dimensions.
	 * 
	 * @param <T> type des objects contenu dans l'ArrayList<ArrayList<T>>
	 * @param tab ArrayList à transformer
	 * @return un tableau à plusieurs dimensions correspondant à
	 *         l'ArrayList<ArrayList<T>>
	 */
	public static final <T> T[][] arrayArrayListToTab(ArrayList<ArrayList<T>> tab) {
		Object[][] res=new Object[tab.size()][tab.get(0).size()];
		ArrayList<T[]> ar=arrayArrayListToArrayListTab(tab);
		int i=0;
		for(T[] tt : ar) {
			res[i]=tt;
			i++;
		}
		return (T[][]) res;
	}


	public static final <T> T[] add(T[] tab1, T[] tab2) {
		/*Object[] ret=new Object[tab1.length + tab2.length];
		int index=0;
		for(int l=tab1.length; index < l; index++) {
			ret[index]=tab1[index];
		}
		for(int i=0, l=tab2.length; i < l; i++) {
			ret[index]=tab1[i];
			index++;
		}*/

		/*Object[] ret=new Object[tab1.length + tab2.length];
		System.arraycopy(tab1, 0, ret, 0, tab1.length);*/
		Object[] ret=agrandirTableau(tab1, tab1.length + tab2.length);
		System.arraycopy(tab2, 0, ret, tab1.length, tab2.length);
		return (T[]) ret;
	}
	public static final <T> T[] add(T[] tab1, T o) {
		/*Object[] ret=new Object[tab1.length + 1];
		int index=0;
		for(int l=tab1.length; index < l; index++) {
			ret[index]=tab1[index];
		}
		ret[index]=o;*/

		/*Object[] ret=new Object[tab1.length + 1];
		System.arraycopy(tab1, 0, ret, 0, tab1.length);*/
		Object[] ret=agrandirTableau(tab1, tab1.length + 1);
		ret[tab1.length]=o;
		return (T[]) ret;
	}


	public static final int profondeurTableau(Object tab) {
		int deep=0;
		Class<?> cl=tab.getClass();
		while(cl.isArray()) {
			cl=cl.getComponentType();
			deep++;
		}
		return deep;
	}


	public static final <T> T[] agrandirTableau(T[] tab, int newTaille) {
		Object tab2=Array.newInstance(tab.getClass().getComponentType(), newTaille);
		System.arraycopy(tab, 0, tab2, 0, Array.getLength(tab));
		return (T[]) tab2;
	}


	/**
	 * TODO : vérifier si ok pour tableaux multi-dimensions
	 * 
	 * Réalise une copie d'un tableau à une dimension.
	 * 
	 * @param tab tableau à copier
	 * @return une copie du tableau
	 */
	public static final <T> T[] copie(T[] tab) {
		Object[] ret=new Object[tab.length];
		ret=Arrays.copyOf(tab, tab.length);
		return (T[]) ret;
	}

	public static final void exit() {
		exit(EC_OK);
	}
	public static final void exit(int ec) {
		sop("ExitCode : " + ec);
		System.exit(ec);
	}
}
