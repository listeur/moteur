package moteur.outils;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

import moteur.Constantes;

/*
 * TODO : ajouter gestion de log avec niveaux d'affichage
 * TODO : permettre de filtrer les niveaux d'affichage (genre tous les debugs et tous les fatals)
 */
public class Log implements Constantes {
	private final String chemin=LOG_FOLDER;
	private String nom="out" + new Date().getTime();
	private final String ext=".log";

	private String nomClass=nom;
	private Path pathOut;
	private LogLevelEnum level;

	// Constructeurs
	public Log() {
		this(LogLevelEnum.ALL, null);
	}

	public Log(LogLevelEnum lvl) {
		this(lvl, null);
	}

	public Log(String nomFichier) {
		this(LogLevelEnum.ALL, nomFichier);
	}

	public Log(LogLevelEnum lvl, String nomFichier) {
		level=lvl;
		// if(nomFichier != null && nomFichier.compareTo("") != 0) {
		if(nomFichier != null && !nomFichier.isEmpty()) {
			nomClass=nomFichier;
		}
		changerSortie(nomClass);
	}

	// Méthodes
	public void log(Object o) {
		if(!verif()) {
			return;
		}
		changerSortie();
		switch(level) {
			case ALL :
			case DEBUG :
			case INFO :
			case WARN :
			case ERROR :
			case FATAL :
				StringBuilder s=new StringBuilder();
				s.append("[LOG]   :");
				s.append(o);
				O.fop(s, pathOut);
			break;
			default :
			break;
		}
	}
	public void debug(Object o) {
		if(!verif()) {
			return;
		}
		StringBuilder s=new StringBuilder();
		switch(level) {
			case ALL :
				s.append("[DEBUG] :");
				s.append(o);
				O.fop(s, pathOut);
			break;
			case DEBUG :
				changerSortie("debug");
				s.append("[DEBUG] :");
				s.append(o);
				O.fop(s, pathOut);
				changerSortie();
			break;
			default :
			break;
		}
	}
	public void info(Object o) {
		if(!verif()) {
			return;
		}
		switch(level) {
			case ALL :
			case INFO :
				StringBuilder s=new StringBuilder();
				s.append("[INFO]  :");
				s.append(o);
				O.fop(s, pathOut);
			break;
			default :
			break;
		}
	}
	public void warn(Object o) {
		if(!verif()) {
			return;
		}
		switch(level) {
			case ALL :
			case WARN :
				StringBuilder s=new StringBuilder();
				s.append("[WARN]  :");
				s.append(o);
				O.fop(s, pathOut);
			break;
			default :
			break;
		}
	}
	public void error(Object o) {
		if(!verif()) {
			return;
		}
		switch(level) {
			case ALL :
			case ERROR :
				StringBuilder s=new StringBuilder();
				s.append("[ERROR] :");
				s.append(o);
				O.fop(s, pathOut);
			break;
			default :
			break;
		}
	}
	public void fatal(Object o) {
		if(!verif()) {
			return;
		}
		switch(level) {
			case ALL :
			case FATAL :
				StringBuilder s=new StringBuilder();
				s.append("[FATAL] :");
				s.append(o);
				O.fop(s, pathOut);
			break;
			default :
			break;
		}
	}

	private boolean verif() {
		Path dossier=Paths.get(chemin);
		try {
			FichierTools.creerDossier(dossier);
		} catch(IOException e) {
			return false;
		}
		return true;
	}

	private void changerSortie() {
		changerSortie(null);
	}
	private void changerSortie(String nomFichier) {
		// if(nomFichier != null && nomFichier.compareTo("") != 0) {
		if(nomFichier != null && !nomFichier.isEmpty()) {
			nom=nomFichier;
		} else {
			nom=nomClass;
		}
		pathOut=Paths.get(chemin, nom + ext);
	}
}
