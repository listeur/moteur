package moteur.outils;

import java.util.Collection;
import java.util.Map;

@SuppressWarnings("unchecked")
public class Random {
	private static final java.util.Random RD=new java.util.Random();

	// Méthodes
	public static final <T> T choisirArray(T[] tab) {
		return tab[RD.nextInt(tab.length)];
	}
	public static final <T> T choisirArray(Collection<T> tab) {
		return (T) choisirArray(tab.toArray());
	}
	public static final <K, V> V choisirArray(Map<K, V> tab) {
		return tab.get(choisirArray(tab.keySet()));
	}

	public static final int entre(int a, int b) {
		int aa=Math.min(a, b);
		int bb=Math.max(a, b);
		return (int) (aa + (RD.nextDouble() * (bb - aa)));
	}
	public static final long entre(long a, long b) {
		long aa=Math.min(a, b);
		long bb=Math.max(a, b);
		return (long) (aa + (RD.nextDouble() * (bb - aa)));
	}
	public static final double entre(double a, double b) {
		double aa=Math.min(a, b);
		double bb=Math.max(a, b);
		return aa + (RD.nextDouble() * (bb - aa));
	}

	// Mélange les éléments d'un array
	public static final <T> T[] shuffle(T[] tab) {
		int counter=tab.length;
		T tmp;
		int index=0;
		while((counter--) >= 0) {
			index=RD.nextInt(counter);

			tmp=tab[counter];
			tab[counter]=tab[index];
			tab[index]=tmp;
		}
		return tab;
	}
}
