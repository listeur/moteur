package moteur.outils;

import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.DirectoryStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import moteur.VueMoteur;
import moteur.io.Fichier;
import moteur.local.Local;

public final class FichierTools {
	private static int cpt=0;	// nombre de fichiers comptés
	private static int cptD=0;	// nombre de dossiers comptés

	public static void delete(Path p) throws IOException {
		if(!Files.exists(p)) {
			throw new NoSuchFileException(p.toString());
		} else {
			Local l=VueMoteur.langue();
			if(Files.isDirectory(p)) {
				/*Files.walkFileTree(p, new SimpleFileVisitor<Path>(){
					@Override
					public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
						delete(file);
						return FileVisitResult.CONTINUE;
					}
				});*/
				try (DirectoryStream<Path> stream=Files.newDirectoryStream(p)) {
					for(Path path : stream) {
						delete(path);
					}
					Files.delete(p);
				} catch(IOException e) {
					throw new IOException(l.e("Impossible de supprimer le dossier (%s) !", p.toString()));
				}
			} else if(Files.isRegularFile(p)) {
				try {
					Files.delete(p);
				} catch(IOException e) {
					throw new IOException(l.e("Impossible de supprimer le fichier (%s) !", p.toString()));
				}
			} else {
				throw new IOException(l.e("Impossible de supprimer le fichier (%s) !", p.toString()));
			}
		}
	}

	public static void copyFichier(Path src, Path dest) throws IOException {
		copyFichier(src, dest, new CopyOption[0]);
	}
	public static void copyFichier(Path src, Path dest, CopyOption... opt) throws IOException {
		Files.copy(src, dest, opt);
	}

	public static void moveFichier(Path src, Path dest) throws IOException {
		moveFichier(src, dest, new CopyOption[0]);
	}
	public static void moveFichier(Path src, Path dest, CopyOption... opt) throws IOException {
		Files.move(src, dest, opt);
	}

	public static synchronized int compterFichiers(Path p) {
		try {
			Files.walkFileTree(p, new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					cpt++;
					return FileVisitResult.CONTINUE;
				}
			});
		} catch(IOException e) {
			cpt=-1;
		}
		int tmp=cpt;
		cpt=0;
		return tmp;
	}
	public static synchronized int compterDossiers(Path p) {
		try {
			Files.walkFileTree(p, new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
					cptD++;
					return FileVisitResult.CONTINUE;
				}
			});
		} catch(IOException e) {
			cptD=-1;
		}
		int tmp=cptD;
		cptD=0;
		return tmp;
	}

	public static int compterFichiers(Path p, final DirectoryStream.Filter<Path> dsf) throws IOException {
		int c=0;
		DirectoryStream.Filter<Path> mdsf=new DirectoryStream.Filter<Path>() {
			@Override
			public boolean accept(Path entry) throws IOException {
				return Files.isDirectory(entry) || dsf.accept(entry);
			}
		};
		try (DirectoryStream<Path> dsp=Files.newDirectoryStream(p, mdsf)) {
			for(Path pp : dsp) {
				if(Files.isDirectory(pp)) {
					c+=compterFichiers(pp, dsf);
				} else {
					c++;
				}
			}
		}
		return c;
	}
	public static int compterFichiers(Path p, FileFilter ff) {
		return compterFichiers(new Fichier(p), ff);
	}
	private static int compterFichiers(Fichier f, FileFilter ff) {
		int c=0;
		Fichier[] lf=f.listFiles(ff);
		for(int i=0, l=lf.length; i < l; i++) {
			if(lf[i].isDirectory()) {
				c+=compterFichiers(lf[i], ff);
			} else {
				c++;
			}
		}
		return c;
	}
	public static int compterFichiers(Path p, FilenameFilter ff) {
		return compterFichiers(new Fichier(p), ff);
	}
	private static int compterFichiers(Fichier f, FilenameFilter ff) {
		int c=0;
		Fichier[] lf=f.listFiles();
		for(int i=0, l=lf.length; i < l; i++) {
			if(lf[i].isDirectory()) {
				c+=compterFichiers(lf[i], ff);
			} else {
				c++;
			}
		}
		return c;
	}

	public static boolean exists(Path p) {
		return Files.exists(p);
	}

	public static boolean isDossier(Path p) {
		return Files.isDirectory(p);
	}
	public static boolean isFichier(Path p) {
		return Files.isRegularFile(p);
	}

	public static void creerFichier(Path p) throws IOException {
		Files.createFile(p);
	}
	public static void creerFichier(Path p, boolean dossiersAussi) throws IOException {
		Path pasDernier=null;
		if(dossiersAussi) {
			pasDernier=p.subpath(0, p.getNameCount() - 1);
		}
		creerDossier(pasDernier);
		creerFichier(p);
	}
	public static void creerDossier(Path p) throws IOException {
		Files.createDirectories(p);
	}
}
