package moteur.outils;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class Bench {
	public static final long JOUR=86_400_000L;
	public static final long HEURE=3_600_000L;
	public static final long MINUTE=60_000L;
	public static final long SECONDE=1_000L;
	public static final long MILLISEC=1L;

	private static int benchNumero=0;

	private long debut=0L;
	private long fin=0L;
	private long debutNano=0L;
	private long finNano=0L;
	private String name;

	// Constructeur
	public Bench() {
		this("Bench-" + benchNumero++);
	}
	public Bench(String nom) {
		name=nom;
	}

	// Méthodes
	public void start() {
		debut=System.currentTimeMillis();
	}
	public long stop() {
		fin=System.currentTimeMillis();
		return duree();
	}
	public long duree() {
		return (fin > debut?fin - debut:0L);
	}
	public String dureeHumaine() {
		return dureeHumaine(duree());
	}
	public static String dureeHumaine(long duree) {
		String s="";
		Date dduree=new Date(duree);

		String jourDF=new SimpleDateFormat("dd").format(dduree);
		String heureDF=new SimpleDateFormat("HH").format(dduree);
		String minuteDF=new SimpleDateFormat("mm").format(dduree);
		String secondeDF=new SimpleDateFormat("ss").format(dduree);
		String millisecDF=new SimpleDateFormat("SSS").format(dduree);

		int jour=new Integer(jourDF) - 1;
		int heure=new Integer(heureDF) - 1;
		int minute=new Integer(minuteDF);
		int seconde=new Integer(secondeDF);
		int millisec=new Integer(millisecDF);

		if(duree >= JOUR) {
			s=duree + " msec (sup à un jour)";
		} else if(jour > 0 && heure < 0) {
			heure+=jour * 24;
			s=heure + "." + minuteDF + " h";
		} else if(heure > 0) {
			s=heure + "." + minuteDF + " h";
		} else if(minute > 0) {
			s=minute + "." + secondeDF + " min";
		} else if(seconde > 0) {
			s=seconde + "." + millisecDF + " sec";
		} else {
			s=millisec + " msec";
		}
		return s;
	}

	public void startNano() {
		debutNano=System.nanoTime();
	}
	public long stopNano() {
		finNano=System.nanoTime();
		return dureeNano();
	}
	public long dureeNano() {
		return (finNano > debutNano?finNano - debutNano:0L);
	}
	public String dureeHumaineNano() {
		return dureeHumaineNano(dureeNano());
	}
	public static String dureeHumaineNano(long duree) {
		String s="";
		long nano=duree % 1_000_000;
		duree/=1_000_000;
		Date dduree=new Date(duree);

		String jourDF=new SimpleDateFormat("dd").format(dduree);
		String heureDF=new SimpleDateFormat("HH").format(dduree);
		String minuteDF=new SimpleDateFormat("mm").format(dduree);
		String secondeDF=new SimpleDateFormat("ss").format(dduree);
		String millisecDF=new SimpleDateFormat("SSS").format(dduree);

		int jour=new Integer(jourDF) - 1;
		int heure=new Integer(heureDF) - 1;
		int minute=new Integer(minuteDF);
		int seconde=new Integer(secondeDF);
		int millisec=new Integer(millisecDF);

		if(duree >= JOUR) {
			s=duree + " nanosec (sup à un jour)";
		} else if(jour > 0 && heure < 0) {
			heure+=jour * 24;
			s=heure + "." + minuteDF + " h";
		} else if(heure > 0) {
			s=heure + "." + minuteDF + " h";
		} else if(minute > 0) {
			s=minute + "." + secondeDF + " min";
		} else if(seconde > 0) {
			s=seconde + "." + millisecDF + " sec";
		} else if(millisec > 9) {
			s=millisec + " msec";
		} else {
			s=millisec + "," + nano + " msec";
		}
		return s;
	}

	/**
	 * Permet de tester le temps d'execution d'une fonction (sans paramètres)
	 * 
	 * @param nb nombre de fois que la boucle doit s'effectuer
	 * @param obj Objet sur lequel la méthode sera appellée
	 * @param methode nom de la méthode qui devra être appellée de obj
	 */
	public void boucle(int nb, Object obj, String methode) {
		boucleIntern(nb, obj, methode, false);
	}
	public void boucle(int nb, Object obj, String methode, boolean pasPrint) {
		boucleIntern(nb, obj, methode, pasPrint);
	}
	private void boucleIntern(int nb, Object obj, String methode, boolean pasPrint) {
		boolean b=false;
		Class<?>[] param=null;
		Method m=null;
		try {
			m=obj.getClass().getDeclaredMethod(methode, param);
		} catch(NoSuchMethodException e) {
			e.printStackTrace();
		} catch(SecurityException e) {
			e.printStackTrace();
		}
		try {
			m.invoke(obj, new Object[0]);
		} catch(Exception e) {
		}
		start();
		for(int i=0; i < nb; i++) {
			try {
				m.invoke(obj, new Object[0]);
			} catch(Exception e) {
				b=true;
				e.printStackTrace();
			}
		}
		stop();
		if(!pasPrint) {
			if(b) {
				O.sopp("Exception !!! ");
			}
			O.sop(this);
		}
	}

	/**
	 * Donne le nom du Bench actuel.
	 * 
	 * @return le nom du Bench actuel
	 */
	public String getName() {
		return name;
	}

	public String toString() {
		if(duree() == 0L) {
			return dureeHumaineNano();
		} else {
			return dureeHumaine();
		}
	}
}
