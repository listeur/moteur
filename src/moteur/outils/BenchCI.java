package moteur.outils;

import java.util.HashMap;
import java.util.Set;

public class BenchCI {
	private BenchEnum mode;
	private HashMap<String, Long> listeBench=new HashMap<>();

	// Constructeurs
	public BenchCI(BenchEnum be) {
		mode=be;
	}
	public BenchCI(BenchEnum be, String nom) {
		this(be);
		doMesure(nom);
	}

	// Méthodes
	public void doMesure(String nom) {
		switch(mode) {
			case MILLI :
				listeBench.put(nom, getMilli());
			break;
			case NANO :
				listeBench.put(nom, getNano());
			break;
		}
	}

	public long getMesure(String nom) {
		return listeBench.get(nom);
	}

	public long getDuree(String prem, String deus) {
		return Math.abs(getMesure(deus) - getMesure(prem));
	}

	public String getDureeHumaine(String prem, String deus) {
		String ret=null;
		switch(mode) {
			case MILLI :
				ret=Bench.dureeHumaine(getDuree(prem, deus));
			break;
			case NANO :
				ret=Bench.dureeHumaineNano(getDuree(prem, deus));
			break;
		}
		return ret;
	}

	public Set<String> getListeKeyMesures() {
		return listeBench.keySet();
	}

	private long getMilli() {
		return System.currentTimeMillis();
	}
	private long getNano() {
		return System.nanoTime();
	}

	/*public String toString() {
		return "";
	}*/
}
