package moteur.outils;

public enum LogLevelEnum {
	// OFF < ALL < DEBUG < INFO < WARN < ERROR < FATAL
	OFF,
	ALL,
	DEBUG,
	INFO,
	WARN,
	ERROR,
	FATAL
}
