package moteur.vue;

import javax.swing.JMenuBar;

public class JMB extends JMenuBar {
	private static final long serialVersionUID=4659189285926642387L;

	// Constructeur
	public JMB() {
		super();
		init();
	}

	// Méthodes
	private void init() {
		//setBorderPainted(false);
	}

	public void focus() {
		requestFocusInWindow();
	}
}
