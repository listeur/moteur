package moteur.vue;

import java.awt.Dimension;

import javax.swing.JTextArea;
import javax.swing.text.Document;

public class JTA extends JTextArea {
	private static final long serialVersionUID=-7783704326981174464L;

	// Constructeurs
	public JTA() {
		super();
	}
	public JTA(String s) {
		super(s);
	}
	public JTA(Document d) {
		super(d);
	}
	public JTA(int row, int col) {
		super(row, col);
	}
	public JTA(String s, int row, int col) {
		super(s, row, col);
	}
	public JTA(Document d, String s, int row, int col) {
		super(d, s, row, col);
	}
	
	// Méthodes
	public void setPreferredSize(int w, int h) {
		setPreferredSize(new Dimension(w, h));
	}
	public void setPreferredWidth(int w) {
		Dimension d=getPreferredSize();
		d.width=w;
		setPreferredSize(d);
	}
	public void setPreferredHeight(int h) {
		Dimension d=getPreferredSize();
		d.height=h;
		setPreferredSize(d);
	}
	
	public void focus() {
		requestFocusInWindow();
	}
}
