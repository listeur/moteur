package moteur.vue;

import java.awt.Window;

import javax.swing.Icon;
import javax.swing.JOptionPane;

import moteur.Constantes;

public class JOP extends JOptionPane implements Constantes {
	private static final long serialVersionUID=8167489730531405016L;

	// Constructeurs
	public JOP() {
		super();
	}
	public JOP(Object o) {
		super(o);
	}
	public JOP(Object o, int mt) {
		super(o, mt);
	}
	public JOP(Object o, int mt, int ot) {
		super(o, mt, ot);
	}
	public JOP(Object o, int mt, int ot, Icon i) {
		super(o, mt, ot, i);
	}
	public JOP(Object o, int mt, int ot, Icon i, Object[] options) {
		super(o, mt, ot, i, options);
	}
	public JOP(Object o, int mt, int ot, Icon i, Object[] options, Object initValue) {
		super(o, mt, ot, i, options, initValue);
	}

	// Méthodes
	public static void alert(String message) {
		alert(message, FENETRE);
	}
	public static void alert(String message, Window f) {
		showMessageDialog(f, message, "Erreur", ERROR_MESSAGE);
	}

	public static void warn(String message) {
		alert(message, FENETRE);
	}
	public static void warn(String message, Window f) {
		showMessageDialog(f, message, "Attention", WARNING_MESSAGE);
	}

	public static void info(String message) {
		info(message, FENETRE);
	}
	public static void info(String message, Window f) {
		showMessageDialog(f, message, "Info", INFORMATION_MESSAGE);
	}

	public static int confirm(String message) {
		return confirm(message, FENETRE);
	}
	public static int confirm(String message, Window f) {
		return showConfirmDialog(f, message, "", YES_NO_OPTION, QUESTION_MESSAGE);
		//return showConfirmDialog(f, message, "", YES_NO_CANCEL_OPTION, QUESTION_MESSAGE);
		//return showConfirmDialog(f, message, "", OK_CANCEL_OPTION, QUESTION_MESSAGE);
	}

	public void focus() {
		requestFocusInWindow();
	}
}
