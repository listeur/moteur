package moteur.vue;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import moteur.Constantes;
import moteur.DPath;
import moteur.outils.O;

public class JF extends JFrame implements Constantes {
	private static final long serialVersionUID=-4958122128418961238L;
	private static final WindowAdapter windowsCloseAdapter;
	static {
		windowsCloseAdapter=new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				O.exit();
			}
		};
	}

	// Construteurs
	public JF() {
		super();
		init();
	}
	public JF(GraphicsConfiguration g) {
		super(g);
		init();
	}
	public JF(String s) {
		super(s);
		init();
	}
	public JF(String s, GraphicsConfiguration g) {
		super(s, g);
		init();
	}

	public JF(Container contentPane) {
		this();
		setContentPane(contentPane);
	}
	public JF(Container contentPane, String s) {
		this(s);
		setContentPane(contentPane);
	}
	public JF(Container contentPane, String s, GraphicsConfiguration g) {
		this(s, g);
		setContentPane(contentPane);
	}

	// Méthodes
	private void init() {
		//setLocale(VueMoteur.langue().getLocale());
		try {	// mettre une icône par défaut
			setIconImage(ImageIO.read(DPath.dPathIS(S + IMG_FOLDER + S + "moteur_icon.png")));
		} catch(IOException e) {
			// new ExceptionDialog(e);	// ne rien faire
			O.sop("Pas d'icône trouvé (ioe)");
		} catch(IllegalArgumentException iae) {
			O.sop("Pas d'icône trouvé (iae)");
		}

		//try {
		//setDefaultCloseOperation(EXIT_ON_CLOSE);
		//} catch(IllegalArgumentException iae) {
		//	setDefaultCloseOperation(DISPOSE_ON_CLOSE);	// HIDE_ON_CLOSE
		//}
		addExitOnWindowClose();
	}
	public void centrer() {
		setLocationRelativeTo(null);
		//Rectangle r=getGraphicsConfiguration().getBounds();
		//setLocation((r.width - getWidth() >> 1) + r.x, (r.height - getHeight() >> 1) + r.y);
	}

	@Override
	public Component add(Component comp) {
		return super.add(comp);
	}
	@Override
	public Component add(Component comp, int index) {
		return super.add(comp, index);
	}
	@Override
	public void add(Component comp, Object constraints) {
		super.add(comp, constraints);
	}
	@Override
	public void add(Component comp, Object constraints, int index) {
		super.add(comp, constraints, index);
	}

	@Override
	public void pack() {
		super.pack();
		setMinimumSize(getPreferredSize());
	}

	public void setPreferredSize(int w, int h) {
		setPreferredSize(new Dimension(w, h));
	}

	public void focus() {
		requestFocusInWindow();
	}

	public void addExitOnWindowClose() {
		addWindowListener(windowsCloseAdapter);
	}
	public void removeExitOnWindowClose() {
		removeWindowListener(windowsCloseAdapter);
	}
}
