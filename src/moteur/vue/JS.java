package moteur.vue;

import javax.swing.JSeparator;

public class JS extends JSeparator {
	private static final long serialVersionUID=-4348196197179673925L;

	// Constructeur
	public JS() {
		super();
	}
	public JS(int orientation) {
		super(orientation);
	}
	
	// Méthodes
	public void focus() {
		requestFocusInWindow();
	}
}
