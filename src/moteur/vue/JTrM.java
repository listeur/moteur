package moteur.vue;

import java.io.File;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.JTree;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;

public class JTrM extends JTree {
	private static final long serialVersionUID=-7345860987876781310L;

	// Constructeurs
	public JTrM() {
		super();
	}
	public JTrM(Object[] value) {
		super(value);
	}
	public JTrM(Vector<?> value) {
		super(value);
	}
	public JTrM(Hashtable<?, ?> value) {
		super(value);
	}
	public JTrM(TreeNode root) {
		super(root);
	}
	public JTrM(TreeModel name) {
		super(name);
	}
	public JTrM(TreeNode root, boolean asksAllowsChildren) {
		super(root, asksAllowsChildren);
	}

	// Méthodes
	public ArrayList<TreeNode> getListeNode() {
		return getListeNode((TreeNode) getModel().getRoot());
	}
	public ArrayList<TreeNode> getListeNode(TreeNode root) {
		ArrayList<TreeNode> liste=new ArrayList<TreeNode>();

		if(root.isLeaf()) {
			liste.add(root);
		} else {
			for(int i=0, l=root.getChildCount(); i<l; i++) {
				liste.addAll(getListeNode(root.getChildAt(i)));
			}
		}
		return liste;
	}

	@Deprecated
	public ArrayList<File> getListeFile(TreeNode root) {
		ArrayList<TreeNode> listeNode=getListeNode(root);
		ArrayList<File> listeFile=new ArrayList<File>(listeNode.size());
		for(TreeNode node : listeNode) {
			listeFile.add(new File(node.toString()));
		}
		return listeFile;
	}

	public void focus() {
		requestFocusInWindow();
	}
}
