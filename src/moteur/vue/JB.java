package moteur.vue;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import moteur.local.Traduisible;

public class JB extends JButton implements Traduisible {
	private static final long serialVersionUID=-8386833258475426815L;

	// Constructeurs
	public JB() {
		super();
		init();
	}
	public JB(Icon i) {
		super(i);
		init();
	}
	public JB(String s) {
		super(s);
		init();
	}
	public JB(Action a) {
		super(a);
		init();
	}
	public JB(String s, Icon i) {
		super(s, i);
		init();
	}

	// Méthodes
	private void init() {
		setFocusPainted(false);

		// si le theme est celui de Java
		if(UIManager.getLookAndFeel().getID() == "Metal") {
			setContentAreaFilled(false);
			addMouseListener(new MouseAdapter() {
				public void mousePressed(MouseEvent e) {
					if(((JB) e.getSource()).isEnabled()) {
						if(SwingUtilities.isLeftMouseButton(e)) {
							setContentAreaFilled(true);
						}
					}
				}
				public void mouseReleased(MouseEvent e) {
					if(((JB) e.getSource()).isEnabled()) {
						if(SwingUtilities.isLeftMouseButton(e)) {
							setContentAreaFilled(false);
						}
					}
				}
			});
		}
	}

	public void setPreferredSize(int w, int h) {
		setPreferredSize(new Dimension(w, h));
	}
	public void setPreferredWidth(int w) {
		Dimension d=getPreferredSize();
		d.width=w;
		setPreferredSize(d);
	}

	@Override
	public String getTraduction() {
		return getText();
	}
	@Override
	public void setTraduction(String trad) {
		setText(trad);	
	}

	public void focus() {
		requestFocusInWindow();
	}
}
