package moteur.vue;

import java.awt.Font;
import java.text.AttributedCharacterIterator.Attribute;
import java.util.Map;

/**
 * TODO : permettre une gestion plus simple des Font en java !
 * 
 * Array (java.awt.font.TextAttribute)
 * 0 [java.awt.font.TextAttribute(family)]
 * 1 [java.awt.font.TextAttribute(weight)]
 * 2 [java.awt.font.TextAttribute(width)]
 * 3 [java.awt.font.TextAttribute(posture)]
 * 4 [java.awt.font.TextAttribute(size)]
 * 5 [java.awt.font.TextAttribute(transform)]
 * 6 [java.awt.font.TextAttribute(superscript)]
 * 7 [java.awt.font.TextAttribute(char_replacement)]
 * 8 [java.awt.font.TextAttribute(foreground)]
 * 9 [java.awt.font.TextAttribute(background)]
 * 10 [java.awt.font.TextAttribute(underline)]
 * 11 [java.awt.font.TextAttribute(strikethrough)]
 * 12 [java.awt.font.TextAttribute(run_direction)]
 * 13 [java.awt.font.TextAttribute(bidi_embedding)]
 * 14 [java.awt.font.TextAttribute(justification)]
 * 15 [java.awt.font.TextAttribute(input method highlight)]
 * 16 [java.awt.font.TextAttribute(input method underline)]
 * 17 [java.awt.font.TextAttribute(swap_colors)]
 * 18 [java.awt.font.TextAttribute(numeric_shaping)]
 * 19 [java.awt.font.TextAttribute(kerning)]
 * 20 [java.awt.font.TextAttribute(ligatures)]
 * 21 [java.awt.font.TextAttribute(tracking)]
 * 
 * @author Listeur
 * 
 */
public class FontM extends Font {
	private static final long serialVersionUID=1964554271204234670L;

	// Constructeurs
	public FontM(Map<? extends Attribute, ?> attributes) {
		super(attributes);
	}
	public FontM(Font font) {
		super(font);
	}
	public FontM(String nom, int style, int size) {
		super(nom, style, size);
	}
}
