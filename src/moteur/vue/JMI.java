package moteur.vue;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JMenuItem;

import moteur.local.Traduisible;
import moteur.outils.O;

public class JMI extends JMenuItem implements Traduisible {
	private static final long serialVersionUID=-8330191698444998804L;

	// Constructeurs
	public JMI() {
		super();
		init();
	}
	public JMI(Icon icon) {
		super(icon);
		init();
	}
	public JMI(String text) {
		super(text);
		init();
	}
	public JMI(Action a) {
		super(a);
		init();
	}
	public JMI(String text, Icon icon) {
		super(text, icon);
		init();
	}
	public JMI(String text, int mnemonic) {
		super(text, mnemonic);
		init();
	}

	// Méthodes
	private void init() {
		addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				O.sop(((JMI) e.getSource()).getText());
			}
		});
	}
	
	@Override
	public String getTraduction() {
		return getText();
	}
	@Override
	public void setTraduction(String trad) {
		setText(trad);	
	}

	public void focus() {
		requestFocusInWindow();
	}
}
