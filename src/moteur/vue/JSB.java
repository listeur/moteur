package moteur.vue;

import javax.swing.JScrollBar;

public class JSB extends JScrollBar {
	private static final long serialVersionUID=3681839936045660145L;

	// Constructeurs
	public JSB() {
		super();
	}
	public JSB(int orientation) {
		super(orientation);
	}
	public JSB(int orientation, int value, int extent, int min, int max) {
		super(orientation, value, extent, min, max);
	}
	
	// Méthodes
	public void focus() {
		requestFocusInWindow();
	}
}
