package moteur.vue;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;

import javax.swing.JPanel;

public class JP extends JPanel {
	private static final long serialVersionUID=2637971330144906021L;

	// Constructeurs
	public JP() {
		super();
	}
	public JP(LayoutManager l) {
		super(l);
	}
	public JP(boolean b) {
		super(b);
	}
	public JP(LayoutManager l, boolean b) {
		super(l, b);
	}

	// Méthodes
	public void setPreferredSize(int w, int h) {
		setPreferredSize(new Dimension(w, h));
	}
	public void setPreferredWidth(int w) {
		Dimension d=getPreferredSize();
		d.width=w;
		setPreferredSize(d);
	}
	public void setPreferredHeight(int h) {
		Dimension d=getPreferredSize();
		d.height=h;
		setPreferredSize(d);
	}

	@Override
	public Component add(Component comp) {
		return super.add(comp);
	}
	@Override
	public Component add(Component comp, int index) {
		return super.add(comp, index);
	}
	@Override
	public void add(Component comp, Object constraints) {
		super.add(comp, constraints);
	}
	@Override
	public void add(Component comp, Object constraints, int index) {
		super.add(comp, constraints, index);
	}

	public void focus() {
		requestFocusInWindow();
	}
}
