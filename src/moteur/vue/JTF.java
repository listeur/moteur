package moteur.vue;

import java.awt.Dimension;

import javax.swing.JTextField;
import javax.swing.text.Document;

public class JTF extends JTextField {
	private static final long serialVersionUID=3707956834441752320L;

	// Constructeurs
	public JTF() {
		super();
	}
	public JTF(String s) {
		super(s);
	}
	public JTF(int i) {
		super(i);
	}
	public JTF(String s, int i) {
		super(s, i);
	}
	public JTF(Document d, String s, int i) {
		super(d, s, i);
	}

	// Méthodes
	public void setPreferredSize(int w, int h) {
		setPreferredSize(new Dimension(w, h));
	}
	public void setPreferredWidth(int w) {
		Dimension d=getPreferredSize();
		d.width=w;
		setPreferredSize(d);
	}

	public void focus() {
		requestFocusInWindow();
	}
}
