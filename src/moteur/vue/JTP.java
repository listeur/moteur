package moteur.vue;


import java.awt.Dimension;

import javax.swing.JTabbedPane;

/**
 * Pour ajouter des onglets.
 * 
 * JTabbedPane tabbedPane=new JTabbedPane();
 * JComponent panel1=makeTextPanel("Panel #1");
 * tabbedPane.addTab("Tab 1", panel1);
 * 
 * @author Listeur
 * 
 */
public class JTP extends JTabbedPane {
	private static final long serialVersionUID=5013154422381822992L;

	// Constructeurs
	public JTP() {
		super();
	}
	public JTP(int tabPlacement) {
		super(tabPlacement);
	}
	public JTP(int tabPlacement, int tabLayoutPolicy) {
		super(tabPlacement, tabLayoutPolicy);
	}

	// Méthode
	public void setPreferredSize(int w, int h) {
		setPreferredSize(new Dimension(w, h));
	}
	public void setPreferredWidth(int w) {
		Dimension d=getPreferredSize();
		d.width=w;
		setPreferredSize(d);
	}
	public void setPreferredHeight(int h) {
		Dimension d=getPreferredSize();
		d.height=h;
		setPreferredSize(d);
	}
	
	public void focus() {
		requestFocusInWindow();
	}
}
