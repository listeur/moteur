package moteur.vue;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JScrollPane;

public class JSP extends JScrollPane {
	private static final long serialVersionUID=-8584687715451295665L;

	// Constructeurs
	public JSP() {
		super();
	}
	public JSP(Component c) {
		super(c);
	}
	public JSP(int vsbPolicy, int hsbPolicy) {
		super(vsbPolicy, hsbPolicy);
	}
	public JSP(Component c, int vsbPolicy, int hsbPolicy) {
		super(c, vsbPolicy, hsbPolicy);
	}

	// Méthodes
	public void setPreferredSize(int w, int h) {
		setPreferredSize(new Dimension(w, h));
	}
	public void setPreferredWidth(int w) {
		Dimension d=getPreferredSize();
		d.width=w;
		setPreferredSize(d);
	}
	public void setPreferredHeight(int h) {
		Dimension d=getPreferredSize();
		d.height=h;
		setPreferredSize(d);
	}

	public void focus() {
		requestFocusInWindow();
	}
}
