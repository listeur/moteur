package moteur.vue;

import java.awt.GridBagConstraints;
import java.awt.Insets;

// http://vip.cs.utsa.edu/classes/java/tutorial/gridbaglayout.html
public class GBC extends GridBagConstraints {
	private static final long serialVersionUID=1641734469070064367L;

	private static final Insets MARGE_DEF=new Insets(0, 0, 0, 0);

	// Constructeurs
	public GBC() {
		//super(RELATIVE, RELATIVE, 1, 1, 0.0, 0.0, CENTER, NONE, new Insets(0,0,0,0), 0, 0);
		super();
	}
	public GBC(int gridx, int gridy, int gridwidth, int gridheight, double weightx, double weighty, int anchor, int fill, Insets insets, int ipadx, int ipady) {
		super(gridx, gridy, gridwidth, gridheight, weightx, weighty, anchor, fill, insets, ipadx, ipady);
	}
	public GBC(int gridx, int gridy, int gridwidth, int gridheight, double weightx, double weighty, int anchor) {
		this(gridx, gridy, gridwidth, gridheight, weightx, weighty, anchor, NONE, MARGE_DEF, 0, 0);
	}
	public GBC(int gridx, int gridy, int gridwidth, int gridheight, double weightx, double weighty) {
		this(gridx, gridy, gridwidth, gridheight, weightx, weighty, CENTER, NONE, MARGE_DEF, 0, 0);
	}
	public GBC(int gridx, int gridy, int gridwidth, int gridheight) {
		this(gridx, gridy, gridwidth, gridheight, 0.0, 0.0, CENTER, NONE, MARGE_DEF, 0, 0);
	}
	public GBC(int gridx, int gridy) {
		this(gridx, gridy, 1, 1, 0.0, 0.0, CENTER, NONE, MARGE_DEF, 0, 0);
	}

	//Méthodes
	public GBC topLeft() {
		anchor=NORTHWEST;
		return this;
	}
	public GBC top() {
		anchor=NORTH;
		return this;
	}
	public GBC topRight() {
		anchor=NORTHEAST;
		return this;
	}
	public GBC left() {
		anchor=WEST;
		return this;
	}
	public GBC centre() {
		anchor=CENTER;
		return this;
	}
	public GBC right() {
		anchor=EAST;
		return this;
	}
	public GBC bottomLeft() {
		anchor=SOUTHWEST;
		return this;
	}
	public GBC bottom() {
		anchor=SOUTH;
		return this;
	}
	public GBC bottomRight() {
		anchor=SOUTHEAST;
		return this;
	}

	public GBC margin(int t, int r, int b, int l) {
		insets=new Insets(t, l, b, r);
		return this;
	}
	public GBC margin(int t, int c, int b) {
		return margin(t, c, b, c);
	}
	public GBC margin(int g, int c) {
		return margin(g, c, g, c);
	}
	public GBC margin(int a) {
		return margin(a, a, a, a);
	}

	public GBC fill() {
		fill=BOTH;
		return this;
	}
	public GBC fillH() {
		fill=HORIZONTAL;
		return this;
	}
	public GBC fillV() {
		fill=VERTICAL;
		return this;
	}

	public GBC poids(double p) {
		weightx=p;
		weighty=p;
		return this;
	}
	public GBC poidsH(double w) {
		weightx=w;
		return this;
	}
	public GBC poidsV(double w) {
		weighty=w;
		return this;
	}

	public GBC hauteur(int h) {
		gridheight=h;
		return this;
	}
	public GBC largeur(int w) {
		gridwidth=w;
		return this;
	}

	public GBC x(int x) {
		gridx=x;
		return this;
	}
	public GBC y(int y) {
		gridy=y;
		return this;
	}

	public String toString() {
		String s="GBC   ";
		s+=" gridx : " + gridx;
		s+=" gridy : " + gridy;
		s+=" gridwidth : " + gridwidth;
		s+=" gridheight : " + gridheight;
		s+=" weightx : " + weightx;
		s+=" weighty : " + weighty;
		s+=" anchor : " + anchor;
		s+=" fill : " + fill;

		return s;
	}
}
