package moteur.vue;

import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

public class JTaM extends JTable {
	private static final long serialVersionUID=8274311917302947947L;

	// Constructeurs
	public JTaM() {
		super();
	}
	public JTaM(TableModel dm) {
		super(dm);
	}
	public JTaM(TableModel dm, TableColumnModel cm) {
		super(dm, cm);
	}
	public JTaM(int numRows, int numColumns) {
		super(numRows, numColumns);
	}
	public JTaM(Vector<?> rowData, Vector<?> columnNames) {
		super(rowData, columnNames);
	}
	public JTaM(Object[][] rowData, Object[] columnNames) {
		super(rowData, columnNames);
	}
	public JTaM(TableModel dm, TableColumnModel cm, ListSelectionModel sm) {
		super(dm, cm, sm);
	}

	// Méthodes
	public void focus() {
		requestFocusInWindow();
	}
}
