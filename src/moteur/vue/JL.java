package moteur.vue;

import java.awt.Dimension;

import javax.swing.Icon;
import javax.swing.JLabel;

import moteur.local.Traduisible;

public class JL extends JLabel implements Traduisible {
	private static final long serialVersionUID=-4344244226873960045L;

	// Constructeurs
	public JL() {
		super();
	}
	public JL(String s) {
		super(s);
	}
	public JL(Icon i) {
		super(i);
	}
	public JL(String s, int h) {
		super(s, h);
	}
	public JL(Icon i, int h) {
		super(i, h);
	}
	public JL(String s, Icon i, int h) {
		super(s, i, h);
	}
	
	// Méthodes
	public void setPreferredSize(int w, int h) {
		setPreferredSize(new Dimension(w, h));
	}
	public void setPreferredWidth(int w) {
		Dimension d=getPreferredSize();
		d.width=w;
		setPreferredSize(d);
	}

	@Override
	public String getTraduction() {
		return getText();
	}
	@Override
	public void setTraduction(String trad) {
		setText(trad);	
	}
	
	public void focus() {
		requestFocusInWindow();
	}
}
