package moteur.vue.custom;

import javax.swing.text.Document;

import moteur.vue.JSP;
import moteur.vue.JTA;

/**
 * Classe qui permet de créer des textarea déjà dans un scroll d'affichage.
 * 
 * @author victor
 * 
 */
public class JTASP extends JSP {
	private static final long serialVersionUID=4096872968044259211L;

	private JTA textArea;

	// Constructeurs
	public JTASP() {
		super();
		textArea=new JTA();
		this.setViewportView(textArea);
	}
	public JTASP(int vsbPolicy, int hsbPolicy) {
		super(vsbPolicy, hsbPolicy);
		textArea=new JTA();
		this.setViewportView(textArea);
	}

	public JTASP(String s) {
		super();
		textArea=new JTA(s);
		this.setViewportView(textArea);
	}
	public JTASP(Document d) {
		super();
		textArea=new JTA(d);
		this.setViewportView(textArea);
	}
	public JTASP(String s, int vsbPolicy, int hsbPolicy) {
		super(vsbPolicy, hsbPolicy);
		textArea=new JTA(s);
		this.setViewportView(textArea);
	}

	// Méthodes
	public JTA getTextArea() {
		return textArea;
	}
}
