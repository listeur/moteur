package moteur.vue.custom;

import java.awt.GridBagLayout;
import java.awt.LayoutManager;

import moteur.vue.JP;

public class JPM extends JP {
	private static final long serialVersionUID=-8329525886319804787L;

	// Constructeur
	public JPM() {
		super(new GridBagLayout());
	}
	public JPM(boolean isDoubleBuffered) {
		super(new GridBagLayout(), isDoubleBuffered);
	}

	// Méthodes
	@Override
	public void setLayout(LayoutManager mgr) {
		resetLayout();
	}
	public void setLayout() {
		resetLayout();
	}
	public void resetLayout() {
		super.setLayout(new GridBagLayout());
	}
}
