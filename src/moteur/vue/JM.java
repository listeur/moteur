package moteur.vue;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Action;
import javax.swing.JMenu;

import moteur.local.Traduisible;
import moteur.outils.O;

public class JM extends JMenu implements Traduisible {
	private static final long serialVersionUID=3646746492908184709L;

	// Constructeurs
	public JM() {
		super();
		init();
	}
	public JM(String s) {
		super(s);
		init();
	}
	public JM(Action a) {
		super(a);
		init();
	}
	public JM(String s, boolean b) {
		super(s, b);
		init();
	}

	// Méthodes
	private void init() {
		//setBorderPainted(false);
		addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				O.sop(((JM) e.getSource()).getText());
			}
		});
	}

	@Override
	public String getTraduction() {
		return getText();
	}
	@Override
	public void setTraduction(String trad) {
		setText(trad);	
	}

	public void focus() {
		requestFocusInWindow();
	}
}
