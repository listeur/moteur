package moteur.config;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import moteur.Constantes;
import moteur.DPath;
import moteur.Prop;
import moteur.PropBadFormatException;
import moteur.VueMoteur;
import moteur.io.Fichier;
import moteur.local.Local;
import moteur.vue.JOP;

public class Config implements Constantes {
	private boolean perso=false;
	private Fichier fileConfig=null;
	private Prop propConfig=null;
	private Local l=VueMoteur.langue();
	private static final Properties PROP_DEF=ConfigEnum.getDefautProperties();

	// Constructeurs
	public Config() {
		Fichier ftmp=DPath.dPathNotNull(CONFIG_FILE_NAME);
		if(ftmp != null && ftmp.exists()) {
			fileConfig=ftmp;

			try {
				propConfig=new Prop(fileConfig, PROP_DEF);
			} catch(FileNotFoundException e) {
				propConfig=null;
			} catch(IOException e) {
				JOP.alert(l.e("Impossible de charger le fichier %s", CONFIG_FILE_NAME));
			}
		}
	}
	public Config(String confPerso) {
		Fichier ftmp=DPath.dPathNotNull(confPerso);
		if(ftmp != null && ftmp.exists()) {
			fileConfig=ftmp;

			try {
				propConfig=new Prop(fileConfig, PROP_DEF);
			} catch(FileNotFoundException e) {
				propConfig=null;
			} catch(IOException e) {
				JOP.alert(l.e("Impossible de charger le fichier %s", confPerso));
			}
			perso=true;
		}
	}

	// Méthodes
	public String get(String clef) {
		String ret=null;
		if(propConfig != null) {
			ret=propConfig.getProperty(clef);
		}
		return ret;
	}
	public boolean set(String clef, String valeur) {
		if(propConfig != null) {
			propConfig.setProperty(clef, valeur);
			try {
				return propConfig.sauver();
			} catch(PropBadFormatException e) {
			} catch(FileNotFoundException e) {
			} catch(IOException e) {
			} finally {
				if(perso) {
					JOP.alert(l.e("Impossible de sauvegarder le fichier %s", fileConfig.getPath()));
				} else {
					JOP.alert(l.e("Impossible de sauvegarder le fichier %s", CONFIG_FILE_NAME));
					//JOP.alert("Impossible de sauvegarder le fichier %s" + CONFIG_FILE_NAME);
				}
			}
		}
		return false;
	}

	/**
	 * Permet de savoir si le fichier config.ini existe ou pas dans le projet
	 * courant et le moteur.
	 * Ne fonctionne qu'avec l'emplacement de config.ini par défaut !
	 * 
	 * @return true si un fichier de configuration est trouvé.
	 */
	public static boolean existConfig() {
		return DPath.existFile(CONFIG_FILE_NAME);
	}
}
