package moteur.config;

import java.util.Properties;

import moteur.Constantes;

public enum ConfigEnum implements Constantes {
	LOCALE("locale", LOCALE_DEFAUT),
	LOCALE_STRING("localeString", LOCALE_STRING_DEFAUT),
	PAYS("pays", PAYS_DEFAUT),
	LANGUE("langue", LANGUE_DEFAUT);

	private String clef;
	private Object defaut;
	private static final Properties propDefaut=new Properties();
	static {
		propDefaut.setProperty(LOCALE.getClef(), LOCALE.getDefaut().toString());
		propDefaut.setProperty(LOCALE_STRING.getClef(), LOCALE_STRING.getDefaut().toString());
		propDefaut.setProperty(PAYS.getClef(), PAYS.getDefaut().toString());
		propDefaut.setProperty(LANGUE.getClef(), LANGUE.getDefaut().toString());
	}

	// Constructeur
	private ConfigEnum(String cle, Object def) {
		clef=cle;
		defaut=def;
	}

	// Méthodes
	public static Properties getDefautProperties() {
		return propDefaut;
	}
	public String getClef() {
		return clef;
	}
	public Object getDefaut() {
		return defaut;
	}
	public String toString() {
		return clef;
	}
}
