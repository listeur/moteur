package moteur;

import java.awt.Component;
import java.awt.Container;
import java.awt.DisplayMode;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import moteur.local.Local;
import moteur.local.Traduisible;
import moteur.outils.O;
import moteur.vue.JF;
import moteur.vue.JM;
import moteur.vue.JMB;
import moteur.vue.JMI;
import moteur.vue.JP;

public class VueMoteur implements Constantes {
	public static final Toolkit TK=Toolkit.getDefaultToolkit();
	private static Local langue=Local.getDefault();

	// Méthodes
	public static void main(String[] args) {
		O.sop(NOM_MOTEUR + " version " + VERSION_STRING);
		O.sop("par " + AUTEUR);
	}

	public static void initLnF() {
		try {
			String lnf=UIManager.getSystemLookAndFeelClassName();
			//lnf = "javax.swing.plaf.metal.MetalLookAndFeel";				// default
			//lnf = "javax.swing.plaf.nimbus.NimbusLookAndFeel";			// moche
			//lnf = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";	// Windows
			UIManager.setLookAndFeel(lnf);
		} catch(ClassNotFoundException e) {
			//O.sop("PAS DE CLASSE TROUVEE POUR LE LOOKnFEEL !");
			new ExceptionDialog(e, "Pas de classe trouvée pour le LookAndFeel !");
		} catch(InstantiationException e) {
			new ExceptionDialog(e);
		} catch(IllegalAccessException e) {
			new ExceptionDialog(e);
		} catch(UnsupportedLookAndFeelException e) {
			new ExceptionDialog(e);
		}
	}
	public static void initLnF(Local l) {
		initLnF();
		setLangue(l);
	}

	/**
	 * Permet de recharger le LooknFeel d'un component s'il a été changé en
	 * cours d'execution.
	 * 
	 * @param c Component dont on veut recharger le LooknFeel
	 */
	public static void rechargeLnF(Component c) {
		SwingUtilities.updateComponentTreeUI(c);
	}

	public static Local langue() {
		return langue;
	}
	public static void setLangue(Local l) {
		langue=l;
		//setLocale(langue.getLocale());
		recur(FENETRE);
	}

	private static void recur(Component jc) {
		if(jc instanceof JF) {
			Container cont=((JF) jc).getContentPane();

			int count=cont.getComponentCount();
			for(int i=0; i < count; i++) {
				recur(cont.getComponent(i));
			}

			JMB jmb=(JMB) ((JF) jc).getJMenuBar();

			if(jmb != null) {
				int count2=jmb.getMenuCount();
				for(int i=0; i < count2; i++) {
					JM m=(JM) jmb.getMenu(i);
					recur(m);

					int count3=m.getItemCount();
					for(int j=0; j < count3; j++) {
						JMI mi=(JMI) m.getItem(j);
						recur(mi);
					}
				}
			}
		}

		if(jc instanceof JP || jc instanceof JF) {
			int count=((Container) jc).getComponentCount();
			for(int i=0; i < count; i++) {
				recur(((Container) jc).getComponent(i));
			}
			return;
		}

		if(jc instanceof Traduisible) {
			String text=((Traduisible) jc).getTraduction();
			if(text != null) {
				((Traduisible) jc).setTraduction(langue.re(text));
			}
		}
	}

	public static final void afficherTailleEcran() {
		double w=TK.getScreenSize().width / TK.getScreenResolution() * 2.54;
		double h=TK.getScreenSize().height / TK.getScreenResolution() * 2.54;
		O.sop(w + " x " + h + " cm");
	}
	public static final void beep() {
		TK.beep();
	}


	public static int getNombreDisplay() {
		GraphicsDevice[] gd=GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();
		return gd.length;
	}

	public static GraphicsConfiguration getGraphConf() {
		return getGraphConf(0);
	}
	public static GraphicsConfiguration getGraphConf(int ecran) {
		GraphicsDevice[] gd=GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();
		if(ecran < 0 || ecran >= gd.length) {
			throw new IllegalArgumentException();
		}
		return gd[ecran].getDefaultConfiguration();
	}

	/*
	 * TODO : problème car FENETRE pas utilisé
	 */
	public static JF setFenetreGraphConf(int ecran) {
		GraphicsConfiguration gc=VueMoteur.getGraphConf(ecran);
		FENETRE.setBounds(gc.getBounds());
		return new JF(gc);
	}

	@Deprecated
	public static void choixEcran() {
		GraphicsDevice[] gd=GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();
		for(int i=0; i < gd.length; i++) {
			DisplayMode dm=gd[i].getDisplayMode();
			O.sop(dm.getWidth() + " x " + dm.getHeight());
			O.sop(gd[i].getIDstring());
			JF jf=new JF(gd[i].getDefaultConfiguration());
			jf.centrer();
			jf.setVisible(true);
		}
	}
	@Deprecated
	public static JF choixEcran(int nb) {
		GraphicsDevice[] gd=GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();
		DisplayMode dm=gd[nb].getDisplayMode();
		O.sop(dm.getWidth() + " x " + dm.getHeight());
		O.sop(gd[nb].getIDstring());
		O.sop(new JF(gd[nb].getDefaultConfiguration()).getBounds());	// java.awt.Rectangle[x=1280,y=-250,width=0,height=0]
		return new JF(gd[nb].getDefaultConfiguration());
	}
}
