package moteur;

import moteur.outils.O;

// TODO : a tester
public class Plugins implements Constantes {
	private String dossierPlugin=null;

	// Constructeur
	public Plugins() {
		this(PLUGIN_FOLDER);
	}

	public Plugins(String dossier) {
		dossierPlugin=dossier;
	}

	// Méthodes
	public void gogogo() {
		O.sop(dossierPlugin);
	}
}

/*	public void test() {
		//Recupere le ClassPath et cast en UrlClassLoader
		URLClassLoader sysLoader=(URLClassLoader) ClassLoader.getSystemClassLoader();
		try {
			URL url=new File("./lib/mp3agic-0.8.1.jar").toURI().toURL();
			Method addURL=URLClassLoader.class.getDeclaredMethod("addURL", new Class<?>[]{URL.class});
			//Permet de mettre la methode addURL accessible car elle est protected
			addURL.setAccessible(true);
			addURL.invoke(sysLoader, new Object[]{url});
		} catch(Exception e) {
			e.printStackTrace();
		}
	}*/

/* 	package pkg;  

class Cls { 

     native double f(int i, String s); 

     static { 

         System.loadLibrary("pkg_Cls"); 

     } 

}  */
