package moteur;

import java.nio.file.Paths;
import java.util.Locale;

import moteur.outils.FichierTools;
import moteur.vue.JF;

/**
 * Liste toutes les constantes utilisées par le moteur.
 * 
 * @author Listeur
 * 
 */
public interface Constantes {
	// Général
	public static final String NOM_MOTEUR="VMoteur";
	public static final double VERSION=1.12_06_140722;
	public static final String VERSION_STRING="1.12.6.140722";
	public static final String AUTEUR="Victor .G";
	public static final String S="/";									// File.separator
	public static final String WS_URL="http://localhost/dropbox/ws/";

	// Debug
	public static final boolean MAISON=FichierTools.exists(Paths.get("C:/Users/Listeur"));

	// Codes sortie du programme
	public static final int EC_OK=0x0;
	public static final int EC_ERROR=-0x1;
	public static final int EC_DIALOG_EXCEPTION=0xDE;
	public static final int EC_FLIC=0x22;				// inutile
	public static final int EC_DEVIL=0x666;				// inutile
	public static final int EC_PABLO=0xCACA;			// inutile
	public static final int EC_BLONDE=0xB00B5;			// inutile
	public static final int EC_MORT=0xDEAD;				// inutile

	// Config defaut
	public static final Locale LOCALE_DEFAUT=new Locale("fr", "FR");	// Locale.FRANCE;
	public static final String LOCALE_STRING_DEFAUT="fr-FR";
	public static final String LANGUE_DEFAUT="fr";
	public static final String PAYS_DEFAUT="FR";

	// Appli
	public static final JF FENETRE=new JF();
	public static final String CONFIG_FILE_NAME="config.ini";
	public static final String LOCAL_FOLDER="local";
	public static final String LOG_FOLDER="log";
	public static final String IMG_FOLDER="img";
	public static final String LIB_FOLDER="lib";
	public static final String PLUGIN_FOLDER="plugin";
}
