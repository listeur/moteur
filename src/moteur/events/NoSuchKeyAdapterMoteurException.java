package moteur.events;

public class NoSuchKeyAdapterMoteurException extends Exception {
	private static final long serialVersionUID=1876506350735329218L;

	// Constructeurs
	public NoSuchKeyAdapterMoteurException() {
		super();
	}
	public NoSuchKeyAdapterMoteurException(String s) {
		super(s);
	}
	public NoSuchKeyAdapterMoteurException(Throwable t) {
		super(t);
	}
	public NoSuchKeyAdapterMoteurException(String s, Throwable t) {
		super(s, t);
	}
	public NoSuchKeyAdapterMoteurException(String s, Throwable t, boolean b1, boolean b2) {
		super(s, t, b1, b2);
	}
}
