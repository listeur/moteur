package moteur.events;

import java.awt.event.KeyEvent;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import moteur.ExceptionDialog;
import moteur.events.extend.KeyAdapterExtend;
import moteur.outils.O;

/**
 * TODO : Avoir toujours le nom de la touche en anglais
 */
public abstract class KeyAdapterMoteur extends KeyAdapterExtend {
	// Méthodes
	@Override
	public void keyPressed(KeyEvent ke) {
	}
	@Override
	public void keyReleased(KeyEvent ke) {
		boolean ctrl=ke.isControlDown();
		boolean alt=ke.isAltDown();
		boolean shift=ke.isShiftDown();

		String nomMethode="key";
		nomMethode+=(ctrl?"Ctrl":"");
		nomMethode+=(alt?"Alt":"");
		nomMethode+=(shift?"Shift":"");

		String keyText="NULL";
		switch(ke.getKeyCode()) {
			case 10 :
				keyText="Enter";
			break;
			case 9 :
				keyText="Tab";
			break;
			case 32 :
				keyText="Space";
			break;
			case 27 :
				keyText="Escape";
			break;
			case 155 :
				keyText="Insert";
			break;
			case 33 :
				keyText="PageUp";
			break;
			case 34 :
				keyText="PageDown";
			break;
			case 36 :
				keyText="Home";
			break;
			case 35 :
				keyText="End";
			break;
			case 8 :
				keyText="Backspace";
			break;
			case 127 :
				keyText="Delete";
			break;
			case 38 :
				keyText="Up";
			break;
			case 39 :
				keyText="Right";
			break;
			case 40 :
				keyText="Down";
			break;
			case 37 :
				keyText="Left";
			break;
			case 48 :
			case 49 :
			case 50 :
			case 51 :
			case 52 :
			case 53 :
			case 54 :
			case 55 :
			case 56 :
			case 57 :
				if(shift) {
					nomMethode=nomMethode.replace("Shift", "");
				}
				// break;
			case 96 :
			case 97 :
			case 98 :
			case 99 :
			case 100 :
			case 101 :
			case 102 :
			case 103 :
			case 104 :
			case 105 :
				keyText="" + ke.getKeyChar();
			break;
			default :
				keyText=KeyEvent.getKeyText(ke.getKeyCode()).replaceAll("[- ]", "");
			break;
		}
		nomMethode+=keyText;

		try {
			Method methode=getClass().getMethod(nomMethode, new Class[] {KeyEvent.class});
			methode.setAccessible(true);
			methode.invoke(this, new Object[] {ke});
		} catch(NoSuchMethodException e) {
			// new ExceptionDialog(e);		// NE RIEN FAIRE
			O.sop("NoSuchKeyAdapterMoteurException : " + nomMethode);
		} catch(SecurityException e) {
			new ExceptionDialog(e);
		} catch(IllegalAccessException e) {
			new ExceptionDialog(e);
		} catch(IllegalArgumentException e) {
			new ExceptionDialog(e);
		} catch(InvocationTargetException e) {
			new ExceptionDialog(e);
		}
	}
	@Override
	public void keyTyped(KeyEvent ke) {
	}
}
