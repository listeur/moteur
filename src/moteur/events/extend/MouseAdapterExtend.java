package moteur.events.extend;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

public abstract class MouseAdapterExtend extends MouseAdapter {
	public void mouseDoubleLeftClick(MouseEvent me) {}
	public void mouseLeftClick(MouseEvent me) {}
	public void mouseRightClick(MouseEvent me) {}
	public void mouseMiddleClick(MouseEvent me) {}
	public void mouseWheelMove(MouseWheelEvent mwe) {}
}
