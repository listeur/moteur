package moteur.events;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.SwingUtilities;

import moteur.events.extend.MouseAdapterExtend;
import moteur.outils.O;

/*
 * TODO : intégrer tous les controles
 */
@SuppressWarnings("unused")
public abstract class MouseAdapterMoteur extends MouseAdapterExtend {
	// Méthodes
	@Override
	public void mouseClicked(MouseEvent me) {
		//boolean ctrl=me.isControlDown();
		//boolean alt=me.isAltDown();
		//boolean shift=me.isShiftDown();

		if(SwingUtilities.isLeftMouseButton(me)) {
			O.sop("clic ");
		} else if(SwingUtilities.isRightMouseButton(me)) {
			O.sop("droit ");
		} else if(SwingUtilities.isMiddleMouseButton(me)) {
			O.sop("milieu ");
		}
		switch(me.getButton()) {
			case MouseEvent.BUTTON1 :
				switch(me.getClickCount()) {
					case 1 :
						mouseLeftClick(me);
					break;
					case 2 :
						mouseDoubleLeftClick(me);
					break;
				}
			break;
			case MouseEvent.BUTTON2 :
				mouseMiddleClick(me);
			break;
			case MouseEvent.BUTTON3 :
				mouseRightClick(me);
			break;
		}
	}
	@Override
	public void mouseEntered(MouseEvent me) {
	}
	@Override
	public void mouseExited(MouseEvent me) {
	}
	@Override
	public void mousePressed(MouseEvent me) {
	}
	@Override
	public void mouseReleased(MouseEvent me) {
	}
	@Override
	public void mouseWheelMoved(MouseWheelEvent mwe) {
		//boolean ctrl=mwe.isControlDown();
		//boolean alt=mwe.isAltDown();
		//boolean shift=mwe.isShiftDown();

		O.sop("rotation : " + mwe.getWheelRotation());

		mouseWheelMove(mwe);
	}
	@Override
	public void mouseDragged(MouseEvent me) {
	}
	@Override
	public void mouseMoved(MouseEvent me) {
	}
}
