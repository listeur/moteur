package moteur.events;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import moteur.ExceptionDialog;

/**
 * D'après une wild idea de Gabin. Permet une mise en place rapide et simplistes
 * d'évènements sur un objet mais ne permettant que l'appel d'une méthode de
 * classe sans paramètres.
 * 
 * @author gabin
 * 
 */
public class MethodInvoker implements ActionListener {
	Method methode;
	Object instance;

	// Constructeur
	public MethodInvoker(String nomMethode, Object obj) {
		instance=obj;
		try {
			methode=obj.getClass().getMethod(nomMethode, new Class[0]);
			methode.setAccessible(true);
		} catch(NoSuchMethodException e) {
			new ExceptionDialog(e, "Cette méthode n'existe pas dans la classe \"" + obj.getClass().getSimpleName() + "\"");
		} catch(SecurityException e) {
			new ExceptionDialog(e);
		}
	}

	// Méthodes
	@Override
	public void actionPerformed(ActionEvent ev) {
		String titre="Impossible d'invoquer la méthode \"" + instance.getClass().getSimpleName() + "." + methode.getName() + "\"";
		try {
			methode.invoke(instance, new Object[0]);
		} catch(IllegalAccessException e) {
			new ExceptionDialog(e, titre);
		} catch(IllegalArgumentException e) {
			new ExceptionDialog(e, titre);
		} catch(InvocationTargetException e) {
			new ExceptionDialog(e, titre);
		}
	}

	/*
	 * notes :
	 * Class[] args2 = new Class[1];
	 * int  avec Integer.TYPE
	 * Date avec Date.class
	 */
}
