package moteur;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import moteur.io.Fichier;

/**
 * DPath permet de retrouver un fichier dynamiquement. Il cherche d'abord le
 * fichier dans le projet en cours, puis s'il ne le trouve pas, va voir si
 * moteur.jar existe avec le fichier en question, puis regarde dans le projet
 * Moteur en dernier recours.
 * 
 * Retourne null s'il ne trouve rien
 * 
 */
public final class DPath {

	// Méthodes
	/**
	 * Retrouve un fichier dynamique entre le projet courant et le moteur.
	 * 
	 * @param path
	 *            chemin relatif (sans "/" au début) du fichier à récupérer
	 * @return le fichier trouvé ou null
	 */
	public static Fichier dPath(String path) {
		Fichier f=null;
//O.sop("---- DPATH ----");
		// appli
		f=new Fichier(path);
//O.sop(f);
		if(!f.exists()) {		// le fichier existe dans le projet ?
			Class<?> classe=path.getClass();
			// jar
			try {				// le fichier existe dans un jar du classpath ?
				if(path.startsWith("/")) {
					f=new Fichier(classe.getResource(path).getPath());
				} else {
					f=new Fichier(classe.getResource("/" + path).getPath());
				}
//O.sop(f);
			} catch(NullPointerException npe) {
//O.sop("xxxx EXCEPT xxxx");
				// moteur
				if(new Fichier("../Moteur/").exists()) {	// le fichier existe dans un dossier Moteur à la racine ?
					f=new Fichier("../Moteur/" + path);
//O.sop(f);
				}
				if(!f.exists()) {
					f=null;
				}
			}
		}
//O.sop(f);
//O.sop("<--- DPATH --->");
		return f;
	}
	/* 
	 * Parseur/local/en.xml
	 * Moteur/local/en.xml
	 * Moteur.jar!/local/en.xml
	 * 
	 * Parseur.jar!/local/en.xml
	 * Moteur.jar!/local/en.xml
	 * Parseur.jar!/Moteur.jar!/local/en.xml	???
	 * Parseur.jar!/Moteur/local/en.xml
	 * 
	 */

	/**
	 * Retrouve un InputStream dynamique entre le projet courant et le moteur.
	 * 
	 * @param path
	 *            chemin relatif (sans "/" au début) du fichier à récupérer
	 * @return l'InputStream pour lire le fichier trouvé ou null
	 */
	public static InputStream dPathIS(String path) {
		Fichier f=null;
		// appli
		f=new Fichier(path);
		if(!f.exists()) {		// le fichier existe dans le projet ?
			Class<?> classe=path.getClass();
			InputStream is=null;

			// jar
			if(path.startsWith("/")) {
				is=classe.getResourceAsStream(path);
			} else {
				is=classe.getResourceAsStream("/" + path);
			}
			if(is != null) {
				return is;
			} else {
				// moteur
				if(new Fichier("../Moteur/").exists()) {	// le fichier existe dans un dossier Moteur à la racine ?
					f=new Fichier("../Moteur/" + path);
				}
				if(!f.exists()) {
					f=null;
				}
			}
		}
		FileInputStream fis=null;
		if(f != null) {
			try {
				fis=new FileInputStream(f);
			} catch(FileNotFoundException e) {
				fis=null;
			}
		}
		return (InputStream) fis;
	}

	/**
	 * Retrouve un fichier (ou un dossier) dynamique entre le projet courant et
	 * le moteur. Si aucun fichier (ou dossier) n'est trouvé, il le crée dans le
	 * projet courant.
	 * 
	 * @param path
	 *            chemin relatif (sans "/" au début) du fichier/dossier à
	 *            récupérer
	 * @return le fichier trouvé ou le fichier/dossier créé
	 */
	public static Fichier dPathNotNull(String path) {
		return dPathNotNull(path, false);
	}

	public static Fichier dPathNotNull(String path, boolean isDossier) {
		Fichier f=dPath(path);
		if(f == null) {
			f=new Fichier(path);
			try {
				if(isDossier) {
					f.mkdir();
				} else {
					f.createNewFile();
				}
			} catch(IOException e) {
				new ExceptionDialog(e);
			}
		}
		return f;
	}

	/**
	 * Test si un fichier existe dans le projet courant et le moteur.
	 * 
	 * @param path
	 *            chemin relatif (sans "/" au début) du fichier à vérifier
	 * @return true si le fichier a été trouvé
	 */
	public static boolean existFile(String path) {
		return DPath.dPath(path) != null;
	}

	/**
	 * Transforme un chemin qui va dans un jar en chemin du fichier dans le jar.
	 * 
	 * @param path
	 *            chemin du fichier contenu dans un jar
	 * @return le chemin converti
	 */
	public static String jarPathFromPath(String path) {
		if(!path.contains("!")) {
			return path.replace("\\", "/");
		}
		return path.split("!")[1].replace("\\", "/");
	}
}
