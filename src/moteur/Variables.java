package moteur;

import moteur.vue.JF;

public final class Variables {
	// Appli
	public static JF FENETRE=new JF();
	public static String CONFIG_FILE_NAME="config.ini";
	public static String LOCAL_FOLDER="local";
	public static String LOG_FOLDER="log";
	public static String IMG_FOLDER="img";
}
