package moteur.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Set;

import moteur.ExceptionDialog;
import moteur.outils.O;

public class HttpRequete {
	private String url;
	private HashMap<String, String> gets=new HashMap<String, String>();
	private HashMap<String, String> posts=new HashMap<String, String>();
	private URLConnection con=null;

	// Constructeur
	public HttpRequete(String adresse) {
		url=adresse;
	}

	// Méthodes
	public boolean addGet(String clef, String valeur) {
		clef=encode(clef);
		if(clef == null) {
			return false;
		}
		valeur=encode(valeur);
		if(valeur == null) {
			valeur="";
		}
		String res=gets.put(clef, valeur);
		return (res != null);
	}
	public boolean addPost(String clef, String valeur) {
		clef=encode(clef);
		if(clef == null) {
			return false;
		}
		valeur=encode(valeur);
		if(valeur == null) {
			valeur="";
		}
		String res=posts.put(clef, valeur);
		return (res != null);
	}

	public void delGet(String clef) {
		clef=encode(clef);
		if(clef != null) {
			gets.remove(clef);
		}
	}
	public void delPost(String clef) {
		clef=encode(clef);
		if(clef != null) {
			posts.remove(clef);
		}
	}

	public void envoyer() {
		OutputStreamWriter writer=null;
		URL u=null;
		try {
			//if(url.contains("?")) {
			//	u=new URL(url + "&" + faireGet());
			//} else {
			u=new URL(url + "?" + faireGet());
			//}
		} catch(MalformedURLException e) {
			new ExceptionDialog(e);
		}
		try {
			con=u.openConnection();
			con.setDoOutput(true);

			writer=new OutputStreamWriter(con.getOutputStream());
			writer.write(fairePost());
			writer.flush();
			writer.close();
		} catch(IOException e) {
			new ExceptionDialog(e);
		}
	}
	public String envoyerRecevoir() {
		envoyer();
		return recevoir();
	}

	public String recevoir() {
		if(con != null) {
			// String tmp="";
			StringBuilder tmp=new StringBuilder();
			BufferedReader reader=null;
			try {
				reader=new BufferedReader(new InputStreamReader(con.getInputStream()));
				String ligne;
				while((ligne=reader.readLine()) != null) {
					O.sop(ligne);
					tmp.append(ligne);
				}
				reader.close();
			} catch(IOException e) {
				new ExceptionDialog(e);
			}
			// return tmp;
			return tmp.toString();
		}
		return null;
	}

	public void reset() {
		url=null;
		gets=new HashMap<String, String>();
		posts=new HashMap<String, String>();
		con=null;
	}

	public void setAdresse(String adresse) {
		url=adresse;
	}
	public String getAdresse() {
		return url;
	}

	private String encode(String s) {
		try {
			return URLEncoder.encode(s, "UTF-8");
		} catch(UnsupportedEncodingException e) {
			return null;
		}
	}
	private String faireGet() {
		Set<String> keys=gets.keySet();
		String tmp="";
		for(String k : keys) {
			String v=gets.get(k);
			tmp+="&" + k + "=" + v;
		}
		return tmp.replaceFirst("&", "");
	}
	private String fairePost() {
		Set<String> keys=posts.keySet();
		String tmp="";
		for(String k : keys) {
			String v=posts.get(k);
			tmp+="&" + k + "=" + v;
		}
		return tmp.replaceFirst("&", "");
	}
}
