package moteur.net;

import java.net.InetAddress;
import java.net.UnknownHostException;

public final class Adresse {
	public static InetAddress localhost() {
		return InetAddress.getLoopbackAddress();
		/*try {
			return InetAddress.getByName("localhost");
		} catch(UnknownHostException e) {
			return null;
		}*/
	}
	
	public static InetAddress monPC() {
		try {
			return InetAddress.getLocalHost();
		} catch(UnknownHostException e) {
			return null;
		}
	}
	
	public static InetAddress unSite(String url) {
		try {
			return InetAddress.getByName(url);
		} catch(UnknownHostException e) {
			return null;
		}
	}
}
