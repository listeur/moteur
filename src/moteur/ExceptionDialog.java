package moteur;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import javax.swing.JDialog;

import moteur.events.MethodInvoker;
import moteur.io.Fichier;
import moteur.local.Local;
import moteur.net.HttpRequete;
import moteur.outils.O;
import moteur.vue.GBC;
import moteur.vue.JB;
import moteur.vue.JL;
import moteur.vue.JOP;
import moteur.vue.JSP;
import moteur.vue.JTA;
import moteur.vue.custom.JPM;

/**
 * D'après le programme de Gabin. Permet de récupérer les exceptions et
 * d'afficher une fenêtre de dialogue avec l'utilisateur.
 * 
 * @author gabin
 */
public class ExceptionDialog extends JDialog implements Constantes {
	private static final long serialVersionUID=4251675949597458923L;

	public static final int SAUV_N_STAY=0;
	public static final int SAUV_N_HIDE=1;
	public static final int SAUV_N_QUIT=2;
	private static final String TITRE_DEFAUT="Une exception a été lancée";
	private static final String TITRE_DEFAUT_CRITIQUE="Une grave exception a été lancée";
	private final Local l=VueMoteur.langue();
	private Exception exception;
	private String texteException="";

	// Constructeurs
	public ExceptionDialog(Exception e) {
		this(e, null, false);
	}
	public ExceptionDialog(Exception e, String titre) {
		this(e, titre, false);
	}
	public ExceptionDialog(Exception e, boolean critique) {
		this(e, null, critique);
	}
	public ExceptionDialog(Exception e, String titre, boolean critique) {
		super(FENETRE, titre, true);
		exception=e;

		texteException=("EXCEPTION :\n" + e.getClass() + " : " + e.getMessage());

		for(StackTraceElement ste : e.getStackTrace()) {
			texteException+="\n" + ste;
		}

		init(titre, critique);
	}

	// Méthodes
	private void init(String titre, boolean critique) {
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setResizable(false);

		JPM panel=new JPM();

		if(titre == null) {
			if(critique) {
				titre=TITRE_DEFAUT_CRITIQUE;
			} else {
				titre=TITRE_DEFAUT;
			}
			titre=l.e(titre);
			setTitle(titre);
		}

		if(!titre.endsWith("!")) {
			titre+=" !";
		}
		JL labelTitre=new JL(titre);
		Font font=labelTitre.getFont().deriveFont(Font.BOLD|Font.ITALIC, labelTitre.getFont().getSize() + 2);
		labelTitre.setFont(font);
		labelTitre.setForeground(Color.RED);
		// labelTitre.setPreferredSize(new Dimension(500, 20));
		panel.add(labelTitre, new GBC(0, 0, 3, 1).margin(20, 5));

		JTA texte=new JTA(texteException);
		texte.setEditable(false);
		JSP scroll=new JSP(texte);
		scroll.setPreferredSize(new Dimension(500, 250));
		panel.add(scroll, new GBC(0, 1, 3, 1).fillH().margin(0, 5));

		JB btFermer=new JB(l.e("Quitter le programme"));
		JB btContinuer=new JB(l.e("Continuer quand même"));
		JB btSauver=new JB(l.e("Sauver le log"));
		JB btEnvoyer=new JB(l.e("Envoyer le log"));

		btFermer.addActionListener(new MethodInvoker("quitterAppli", this));
		btContinuer.addActionListener(new MethodInvoker("continuerQuandMeme", this));
		btSauver.addActionListener(new MethodInvoker("sauverLog", this));
		btEnvoyer.addActionListener(new MethodInvoker("envoyerLog", this));

		panel.add(btSauver, new GBC(0, 2).margin(5, 10).poidsH(1.0).left());
		panel.add(btContinuer, new GBC(1, 2).margin(5).poidsH(1.0).right());
		panel.add(btFermer, new GBC(2, 2).margin(5, 10, 5, 0));
		//panel.add(btEnvoyer, new GBC(0, 3).margin(0, 10, 5).poidsH(1.0).left());

		if(critique) {
			btContinuer.setEnabled(false);
			addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosing(WindowEvent e) {
					quitterAppli();		// quitter l'appli lorsque l'on ferme la fenetre ExceptionDialog
				}
			});
		}

		add(panel);
		pack();
		centrer();
		setVisible(true);
	}

	public void continuerQuandMeme() {
		setVisible(false);
	}

	public void quitterAppli() {
		System.exit(EC_DIALOG_EXCEPTION);
	}

	public void sauverLog() {
		sauverLog(SAUV_N_STAY);
	}
	public void sauverLog(int stat) {
		try {
			Fichier dossier=new Fichier(LOG_FOLDER);
			if(!dossier.exists()) {
				dossier.mkdir();
			}

			long time=new Date().getTime();
			String nomFile=exception.getClass().getSimpleName() + time;

			Fichier logfile=new Fichier(LOG_FOLDER + S + nomFile + ".log");
			logfile.createNewFile();

			FileOutputStream fos=new FileOutputStream(logfile);
			fos.write(texteException.replace("\n", "\r\n").getBytes());
			fos.close();

			JOP.info(l.e("Fichier sauvegardé."), this);
		} catch(IOException e) {
			// new ExceptionDialog(e, "Impossible de sauvegarder le log");
			JOP.alert(l.e("Impossible de sauvegarder le log !"), this);
		}
		switch(stat) {
			case SAUV_N_HIDE :
				continuerQuandMeme();
			break;
			case SAUV_N_QUIT :
				quitterAppli();
			break;
		}
	}

	public void envoyerLog() {
		HttpRequete req=new HttpRequete(WS_URL);
		req.addGet("logGet", texteException);
		req.addPost("log", texteException);
		req.envoyer();
		O.sop(req.recevoir());

		JOP.info(l.e("Fichier envoyé."), this);
	}

	private void centrer() {
		Rectangle r=getParent().getBounds();
		Point p=new Point((r.width - getWidth() >> 1) + r.x, (r.height - getHeight() >> 1) + r.y);
		if(p.x < 0) {
			p.x=0;
		}
		if(p.y < 0) {
			p.y=0;
		}
		setLocation(p);
	}
}
