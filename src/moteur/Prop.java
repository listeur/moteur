package moteur;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

import moteur.io.Fichier;

public class Prop extends Properties {
	private static final long serialVersionUID=608632802203129931L;

	private static final int TYPE_DEF=-1;
	public static final int TYPE_XML=0;
	public static final int TYPE_CMD=1;

	private Fichier propFichier=null;
	private int type=TYPE_DEF;

	// Constructeurs
	public Prop(Fichier f) throws PropBadFormatException, FileNotFoundException, IOException {
		this(f, TYPE_DEF);
	}
	public Prop(Fichier f, Properties defaults) throws PropBadFormatException, FileNotFoundException, IOException {
		this(f, defaults, TYPE_DEF);
	}
	public Prop(Fichier f, int t) throws PropBadFormatException, FileNotFoundException, IOException {
		super();
		propFichier=f;
		switch(t) {
			case TYPE_XML :
			case TYPE_CMD :
				type=t;
			break;
			default :
				type=TYPE_DEF;
			break;
		}
		charger();
	}
	public Prop(Fichier f, Properties defaults, int t) throws PropBadFormatException, FileNotFoundException, IOException {
		super(defaults);
		propFichier=f;
		switch(t) {
			case TYPE_XML :
			case TYPE_CMD :
				type=t;
			break;
			default :
				type=TYPE_DEF;
			break;
		}
		charger();
	}

	// Méthodes
	@SuppressWarnings("resource")
	public void charger() throws PropBadFormatException, FileNotFoundException, IOException {
		InputStream is=null;
		try {
			is=new FileInputStream(propFichier);
		} catch(FileNotFoundException e) {
			is=getClass().getResourceAsStream(DPath.jarPathFromPath(propFichier.getPath()));
		}
		if(type == TYPE_DEF) {
			try {
				loadFromXML(is);
				type=TYPE_XML;
			} catch(InvalidPropertiesFormatException e) {
				/*
				 * TODO : revoir la manière de détecter le fichier vide, et de le réouvrir
				 */
				try {
					load(is);
					type=TYPE_CMD;
				} catch(IOException ioe) {	// si n'arrive encore pas à charger
					if(ioe.getMessage().compareTo("Stream Closed") == 0) {	// si le fichier est vide
						type=TYPE_CMD;
						sauver();	// sauvegarder le fichier
						try {		// puis le réouvrir
							is=new FileInputStream(propFichier);
						} catch(FileNotFoundException fnfe) {
							is=getClass().getResourceAsStream(DPath.jarPathFromPath(propFichier.getPath()));
						}
						load(is);	// réessayer de le lire
					} else {
						throw ioe;	// relancer l'exception pour tout autres problèmes
					}
				}
			}
		} else {
			switch(type) {
				case TYPE_XML :
					loadFromXML(is);
				break;
				case TYPE_CMD :
					load(is);
				break;
				default :
					is.close();
					throw new PropBadFormatException();
			}
		}
		is.close();
	}

	public boolean sauver() throws PropBadFormatException, FileNotFoundException, IOException {
		return sauver(type, null);
	}
	public boolean sauver(int t) throws PropBadFormatException, FileNotFoundException, IOException {
		return sauver(t, null);
	}
	public boolean sauver(String commentaire) throws PropBadFormatException, FileNotFoundException, IOException {
		return sauver(type, commentaire);
	}
	public boolean sauver(int t, String commentaire) throws PropBadFormatException, FileNotFoundException, IOException {
		FileOutputStream fos=new FileOutputStream(propFichier);
		switch(t) {
			case TYPE_XML :
				storeToXML(fos, commentaire);
			break;
			case TYPE_CMD :
				store(fos, commentaire);
			break;
			default :
				fos.close();
				throw new PropBadFormatException();
		}
		fos.close();
		return true;
	}
}
