package moteur.io;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import moteur.outils.FichierTools;

public class Fichier extends File {
	private static final long serialVersionUID=4899316045742321613L;

	// Constructeurs
	public Fichier(String pathname) {
		super(pathname);
	}
	public Fichier(URI uri) {
		super(uri);
	}
	public Fichier(String parent, String child) {
		super(parent, child);
	}
	public Fichier(File parent, String child) {
		super(parent, child);
	}
	public Fichier(Path p) {
		this(p.toString());
	}
	private Fichier(File leMeme) {
		this(leMeme.getPath());
	}

	// Méthodes
	@Override
	public Fichier[] listFiles() {
		File[] lf=super.listFiles();
		if(lf != null) {
			Fichier[] lf2=new Fichier[lf.length];
			for(int i=0; i < lf.length; i++) {
				lf2[i]=new Fichier(lf[i]);
			}
			return lf2;
		} else {
			return null;
		}
	}
	@Override
	public Fichier[] listFiles(FileFilter filter) {
		File[] lf=super.listFiles(filter);
		if(lf != null) {
			Fichier[] lf2=new Fichier[lf.length];
			for(int i=0; i < lf.length; i++) {
				lf2[i]=new Fichier(lf[i]);
			}
			return lf2;
		} else {
			return null;
		}
	}
	@Override
	public Fichier[] listFiles(FilenameFilter filter) {
		File[] lf=super.listFiles(filter);
		if(lf != null) {
			Fichier[] lf2=new Fichier[lf.length];
			for(int i=0; i < lf.length; i++) {
				lf2[i]=new Fichier(lf[i]);
			}
			return lf2;
		} else {
			return null;
		}
	}

	@Override
	public boolean delete() {
		return delete(this);
	}
	public boolean delete(boolean nonRecur) {
		if(nonRecur) {
			return super.delete();
		} else {
			return delete(this);
		}
	}
	public static boolean delete(Fichier f) {
		try {
			FichierTools.delete(f.toPath());
		} catch(IOException e) {
			return false;
		}
		return true;
	}

	public boolean copyFichier(Fichier dest) {
		return copyFichier(this, dest);
	}
	public static boolean copyFichier(Fichier src, Fichier dest) {
		try {
			FichierTools.copyFichier(src.toPath(), dest.toPath());
		} catch(IOException e) {
			return false;
		}
		return true;
	}

	public boolean moveFichier(Fichier dest) {
		return moveFichier(this, dest);
	}
	public static boolean moveFichier(Fichier src, Fichier dest) {
		try {
			FichierTools.moveFichier(src.toPath(), dest.toPath());
		} catch(IOException e) {
			return false;
		}
		return true;
	}

	@Override
	public boolean exists() {
		return Files.exists(toPath());
	}
}
