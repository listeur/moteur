package moteur.io;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public enum CharsetEnum {
	ASCII(StandardCharsets.US_ASCII),
	ISO_8859_1(StandardCharsets.ISO_8859_1),
	UTF8(StandardCharsets.UTF_8);

	private Charset charset;

	CharsetEnum(Charset c) {
		charset=c;
	}

	public Charset get() {
		return charset;
	}
}
