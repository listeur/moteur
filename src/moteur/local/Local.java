package moteur.local;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;

import moteur.Constantes;
import moteur.DPath;
import moteur.ExceptionDialog;
import moteur.Prop;
import moteur.config.Config;

/**
 * TODO : faciliter la création d'une langue
 * 
 */
public class Local implements Constantes {
	private static HashMap<String, String> listeIds=new HashMap<>();
	private static HashMap<String, Object[]> secondaire=new HashMap<>();

	//private Config config;
	private Locale locale;
	private String localeS;

	private Prop propDef;
	private Prop prop;

	// Constructeurs
	public Local() {
		Locale.setDefault(LOCALE_DEFAUT);

		if(Config.existConfig()) {
			Config config=new Config();

			String ll=config.get("localeString");
			localeS=(ll == null?LOCALE_STRING_DEFAUT:ll);

			String[] localeSS=localeS.split("[-_]");
			try {
				locale=new Locale(localeSS[0], localeSS[1]);
			} catch(NullPointerException e) {
				locale=LOCALE_DEFAUT;
				localeS=LOCALE_STRING_DEFAUT;
			} catch(ArrayIndexOutOfBoundsException e) {	// si new Locale() == und
				locale=LOCALE_DEFAUT;
				localeS=LOCALE_STRING_DEFAUT;
			}
		} else {
			locale=LOCALE_DEFAUT;
			localeS=LOCALE_STRING_DEFAUT;
		}

		init();
	}

	public Local(Locale l) {
		Locale.setDefault(LOCALE_DEFAUT);

		localeS=l.toLanguageTag();
		//O.sop(localeS);

		String[] localeSS=localeS.split("[-_]");
		try {
			locale=new Locale(localeSS[0], localeSS[1]);
		} catch(NullPointerException e) {
			locale=LOCALE_DEFAUT;
			localeS=LOCALE_STRING_DEFAUT;
		} catch(ArrayIndexOutOfBoundsException e) {	// si new Locale() == und
			locale=LOCALE_DEFAUT;
			localeS=LOCALE_STRING_DEFAUT;
		}

		init();
	}

	// constructeur rapide pour la Local par defaut
	private Local(int def) {
		Locale.setDefault(LOCALE_DEFAUT);

		locale=LOCALE_DEFAUT;
		localeS=LOCALE_STRING_DEFAUT;

		init();
	}

	// Méthodes
	private void init() {
		try {
			//O.sop("defaut   : " + DPath.dPath(S + LOCAL_FOLDER + LOCALE_STRING_DEFAUT + ".xml"));
			propDef=new Prop(DPath.dPath(S + LOCAL_FOLDER + S + LOCALE_STRING_DEFAUT + ".xml"));
		} catch(FileNotFoundException e) {
			new ExceptionDialog(e);
		} catch(IOException e) {
			new ExceptionDialog(e);
		}

		//O.sop("fichier  : " + DPath.dPath(LOCAL_FOLDER + localeS + ".xml"));
		try {
			try {
				prop=new Prop(DPath.dPath(LOCAL_FOLDER + S + localeS + ".xml"));
			} catch(NullPointerException e) {	// si fichier == null
				prop=new Prop(DPath.dPathNotNull(LOCAL_FOLDER + S + localeS + ".xml"));
				sauveLangue();
			}
		} catch(IOException e) {
			prop=propDef;
		}
	}

	public String getLocaleString() {
		return localeS;
	}
	public Locale getLocale() {
		return locale;
	}

	public void a(String cle, String val) {
		prop.setProperty(cle, val);
		sauveLangue();
	}

	public String e(String cle) {
		String ret=prop.getProperty(cle, propDef.getProperty(cle));
		if(ret == null && localeS.equals(LOCALE_STRING_DEFAUT)) {
			aDef(cle, cle);
		}
		listeIds.put(ret, cle);
		return (ret == null)?cle:ret;
	}
	public String e(String cle, Object... args) {
		String ret=e(cle);
		//String ret2=ret;
		if(args.length > 0) {
			ret=String.format(ret, args);
			//for(int i=0, l=args.length; i < l; i++) {
			//	ret2+=args[i].hashCode();
			//}
			secondaire.put(cle, args);
		}
		//listeIds.put(ret2, cle);
		return ret;
	}

	public String re(String text) {
		String ret=text;
		if(listeIds.containsKey(text)) {
			String cle=listeIds.get(text);
			ret=e(cle);
			if(secondaire.containsKey(cle)) {
				ret=e(cle, secondaire.get(cle));
				//secondaire.remove(cle);
			}
			//listeIds.remove(text);
		}
		return ret;
	}

	public void resetMemoire() {
		listeIds=new HashMap<>();
		secondaire=new HashMap<>();
	}

	private void aDef(String cle, String val) {
		propDef.setProperty(cle, val);
		try {
			propDef.sauver(Prop.TYPE_XML);
		} catch(FileNotFoundException e) {
			new ExceptionDialog(e);
		} catch(IOException e) {
			new ExceptionDialog(e);
		}
	}

	private void sauveLangue() {
		try {
			prop.sauver(Prop.TYPE_XML);
		} catch(FileNotFoundException e) {
			new ExceptionDialog(e);
		} catch(IOException e) {
			new ExceptionDialog(e);
		}
	}

	public static Local getDefault() {
		return new Local(0);
	}
}
