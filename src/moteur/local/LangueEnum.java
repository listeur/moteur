package moteur.local;

import java.util.Locale;

public enum LangueEnum {
	ENGLISH("en-GB", "English"),
	FRANCAIS("fr-FR", "Français"),
	JAPONNAIS("ja-JP", "日本人");

	private String ref;
	private String nom;
	private Locale loc;

	LangueEnum(String r) {
		ref=r;
		nom=r;
		loc=new Locale(r);
	}

	LangueEnum(String r, String n) {
		ref=r;
		nom=n;
		loc=new Locale(r);
	}

	public String getRef() {
		return ref;
	}
	public String getNom() {
		return nom;
	}
	public Locale getLocale() {
		return loc;
	}

	public String toString() {
		return nom;
	}
}
