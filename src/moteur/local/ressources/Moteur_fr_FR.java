package moteur.local.ressources;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListResourceBundle;
import java.util.Map.Entry;
import java.util.Set;

import moteur.Constantes;
import moteur.DPath;
import moteur.ExceptionDialog;
import moteur.Prop;
import moteur.outils.O;

public class Moteur_fr_FR extends ListResourceBundle implements Constantes {
	private static final String RESFILE="fr-FR.xml";
	static final Object[][] contents;/*= {	{"Fichier", "Moteur"},
									{"Aide", "Aide"},
									{"Enregistrer", "Enregistrer"},
									{"?", "?"},
									{"Bouton", "Bouton M"},
									{"A Propos", "A Propos"},
									{"Ouvrir", "Ouvrir"},
									{"Fermer", "Fermer"},};*/

	static {
		Prop prop=null;

		try {
			prop=new Prop(DPath.dPath(S + LOCAL_FOLDER + S + RESFILE));
		} catch(FileNotFoundException e) {
			new ExceptionDialog(e);
		} catch(IOException e) {
			new ExceptionDialog(e);
		}

		ArrayList<ArrayList<Object>> aro=new ArrayList<ArrayList<Object>>(); // O.tabToArrayList(contents);

		Set<Entry<Object, Object>> propSet=prop.entrySet();
		Iterator<Entry<Object, Object>> it=propSet.iterator();
		while(it.hasNext()) {
			Entry<Object, Object> entree=it.next();

			ArrayList<Object> ent=new ArrayList<Object>(2);
			ent.add(entree.getKey());
			ent.add(entree.getValue());

			aro.add(ent);
		}

		Object[][] arot=O.arrayArrayListToTab(aro);

		contents=arot;
	}

	// Méthodes
	@Override
	protected Object[][] getContents() {
		return contents;
	}
}
