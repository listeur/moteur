package moteur.local.ressources;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListResourceBundle;
import java.util.Set;
import java.util.Map.Entry;

import moteur.Constantes;
import moteur.DPath;
import moteur.ExceptionDialog;
import moteur.Prop;
import moteur.outils.O;

public class Moteur_en_GB extends ListResourceBundle implements Constantes {
	private static final String RESFILE="en-GB.xml";
	static final Object[][] contents;/*= {	{"Fichier", "File"},
										{"Aide", "Help"},
										{"Enregistrer", "Save"},
										{"?", "?"},
										{"A Propos", "About"},
										{"Ouvrir", "Open"},
										{"Fermer", "Close"},};*/

	static {
		Prop prop=null;

		try {
			prop=new Prop(DPath.dPath(S + LOCAL_FOLDER + S + RESFILE));
		} catch(FileNotFoundException e) {
			new ExceptionDialog(e);
		} catch(IOException e) {
			new ExceptionDialog(e);
		}

		ArrayList<ArrayList<Object>> aro=new ArrayList<ArrayList<Object>>(); // O.tabToArrayList(contents);

		Set<Entry<Object, Object>> propSet=prop.entrySet();
		Iterator<Entry<Object, Object>> it=propSet.iterator();
		while(it.hasNext()) {
			Entry<Object, Object> entree=it.next();

			ArrayList<Object> ent=new ArrayList<Object>(2);
			ent.add(entree.getKey());
			ent.add(entree.getValue());

			aro.add(ent);
		}

		Object[][] arot=O.arrayArrayListToTab(aro);

		contents=arot;
	}

	// Méthodes
	@Override
	protected Object[][] getContents() {
		return contents;
	}
}
