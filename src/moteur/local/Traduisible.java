package moteur.local;

public interface Traduisible {
	/**
	 * Permet de récupérer le teste à traduire.
	 * 
	 * @return string texte traduisible du Component
	 */
	public String getTraduction();
	/**
	 * Permet de changer le texte traduisible par la traduction donnée en
	 * paramètre.
	 * 
	 * @param traduction String de la traduction correspondante au texte donné
	 *            par getTraduction()
	 */
	public void setTraduction(String traduction);
}
