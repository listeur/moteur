package moteur.local;

import java.util.Locale;
import java.util.ResourceBundle;

public class RB {
	private static final String BUNDLE="moteur.local.ressources.Moteur";
	private ResourceBundle ressource;

	// Constructeurs
	public RB() {
		ressource=ResourceBundle.getBundle(BUNDLE, Locale.getDefault());
	}
	public RB(Locale l) {
		ressource=ResourceBundle.getBundle(BUNDLE, l);
	}
	public RB(String bundle, Locale l) {
		ressource=ResourceBundle.getBundle(bundle, l);
	}

	// Méthodes
	public ResourceBundle getRB() {
		return ressource;
	}

	public String get(String motclef) {
		return ressource.getString(motclef);
	}

	public Object getObj(String motclef) {
		return ressource.getObject(motclef);
	}
}
