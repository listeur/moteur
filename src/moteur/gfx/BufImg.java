package moteur.gfx;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.IndexColorModel;
import java.awt.image.WritableRaster;
import java.util.Hashtable;

/**
 * 
 * @TODO : integrer this classe à la classe ImageIO
 * 
 * @TODO : gerrer des images de "texture" avec plusieurs images en une avec
 *       "getSubimage()"
 * 
 * @author victor
 * 
 */
public class BufImg extends BufferedImage {

	// Constructeurs
	public BufImg(ColorModel cm, WritableRaster raster, boolean isRasterPremultiplied, Hashtable<?, ?> properties) {
		super(cm, raster, isRasterPremultiplied, properties);
	}
	public BufImg(int width, int height, int imageType) {
		super(width, height, imageType);
	}
	public BufImg(int width, int height, int imageType, IndexColorModel cm) {
		super(width, height, imageType, cm);
	}
	public BufImg(BufferedImage bi) {
		//this(bi.getWidth(), bi.getHeight(), bi.getType());
		//this(bi.getColorModel(), bi.getRaster(), bi.isAlphaPremultiplied(), null);
		this(bi.getColorModel(), bi.copyData(null), bi.isAlphaPremultiplied(), null);

		/*Hashtable<String, Object> ht=new Hashtable<>();
		String[] props=bi.getPropertyNames();
		for(String nom : props) {
			ht.put(nom, bi.getProperty(nom));
		}*/
	}
}
