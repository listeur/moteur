package moteur;

import java.io.IOException;

public class PropBadFormatException extends IOException {
	private static final long serialVersionUID=-2822909516892175292L;

	// Constructeurs
	public PropBadFormatException() {
		super();
	}
	public PropBadFormatException(String s) {
		super(s);
	}
	public PropBadFormatException(Throwable t) {
		super(t);
	}
	public PropBadFormatException(String s, Throwable t) {
		super(s, t);
	}
}
